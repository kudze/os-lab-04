#include <iostream>

#include "src/real_machine.h"

using namespace std;

int main(int argc, char** argv) {
    cout << "Sveiki atvyke i laboratorinį 04!" << endl << endl;

    cout << "Kuriama mašina..." << endl;
    auto machine = new RealMachine(argc >= 2 && argv[1][0] == '1');

    cout << "Paleidžiama mašina..." << endl;
    machine->run();

    cout << "Trinama mašina..." << endl;
    delete machine;
}
