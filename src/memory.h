//
// Created by kudze on 2022-03-24.
//

#ifndef LAB04_MEMORY_H
#define LAB04_MEMORY_H

#include <cstdint>
#include <array>

#include "segments.h"

#define MEMORY_BLOCK_COUNT 256          //How many blocks in memory.
#define MEMORY_BLOCK_SIZE 16            //How many words in block.
#define MEMORY_WORD_COUNT 4096              //How many total words in memory.

#define MEMORY_USER_BLOCK_OFFSET 0
#define MEMORY_USER_BLOCK_COUNT 192
#define MEMORY_SUPERVISOR_BLOCK_OFFSET MEMORY_USER_BLOCK_COUNT
#define MEMORY_SUPERVISOR_BLOCK_COUNT 64

typedef uint8_t block_address_t; // Range [0, 255]
typedef uint8_t word_address_t; // Range [0, 15]

typedef uint32_t memory_word_t;
using memory_block_t = std::array<memory_word_t, MEMORY_BLOCK_SIZE>;

class MemoryInterface {
public:
    virtual ~MemoryInterface() = default;

    virtual void writeWord(block_address_t blockAddress, word_address_t wordAddress, memory_word_t value) = 0;
    virtual void writeWordCombined(uint16_t combinedAddress, memory_word_t value);
    virtual void writeBlock(block_address_t blockAddress, memory_block_t const& block) = 0;
    virtual void writeBlockString(block_address_t blockAddress, std::string const& message);
    virtual memory_word_t readWord(block_address_t blockAddress, word_address_t wordAddress) const = 0;
    virtual memory_word_t readWordCombined(uint16_t combinedAddress) const;
    virtual memory_block_t readBlock(block_address_t blockAddress) const = 0;
};

class Memory;

class UserMemory : public MemoryInterface {
    Memory* memory;
public:
    explicit UserMemory(Memory* memory);

    void writeWord(block_address_t blockAddress, word_address_t wordAddress, memory_word_t value) override;
    void writeBlock(block_address_t blockAddress, memory_block_t const& block) override;
    memory_word_t readWord(block_address_t blockAddress, word_address_t wordAddress) const override;
    memory_block_t readBlock(block_address_t blockAddress) const override;

    void print() const;

    Memory *getMemory() const;
};

class SupervisorMemory : public MemoryInterface {
    Memory* memory;
public:
    explicit SupervisorMemory(Memory* memory);

    void writeWord(block_address_t blockAddress, word_address_t wordAddress, memory_word_t value) override;
    void writeBlock(block_address_t blockAddress, memory_block_t const& block) override;
    memory_word_t readWord(block_address_t blockAddress, word_address_t wordAddress) const override;
    memory_block_t readBlock(block_address_t blockAddress) const override;

    void print() const;

    Memory *getMemory() const;
};

class Memory : public MemoryInterface {
    std::array<memory_block_t, MEMORY_BLOCK_COUNT> memory = {0};

    UserMemory* userMemory;
    SupervisorMemory* supervisorMemory;

public:
    /**
     * Returns true if all words in block = 0.
     *
     * @param block
     * @return
     */
    static bool isMemoryBlockZeroed(memory_block_t block);

    Memory();
    virtual ~Memory();

    void writeWord(block_address_t blockAddress, word_address_t wordAddress, memory_word_t value) override;
    void writeBlock(block_address_t blockAddress, memory_block_t const& block) override;
    memory_word_t readWord(block_address_t blockAddress, word_address_t wordAddress) const override;
    memory_block_t readBlock(block_address_t blockAddress) const override;

    UserMemory *getUserMemory() const;
    SupervisorMemory *getSupervisorMemory() const;
};


#endif //LAB04_MEMORY_H
