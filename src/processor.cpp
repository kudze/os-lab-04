//
// Created by kudze on 2022-03-24.
//

#include "real_machine.h"

#include <iostream>

using namespace std;

Processor::Processor(RealMachine *machine) : machine(machine) {
    this->r1 = new Int32Register(0);
    this->r2 = new Int32Register(0);
    this->rr = new Int32Register(0);

    this->ic = new UInt16Register(0);
    this->c = new UInt32Register(0);
    this->ss = new UInt8Register(0);
    this->cf = new UInt8Register(0);
    this->ti = new UInt8Register(0);
    this->si = new UInt8Register(SI_REGISTER_VALUE_OK);
    this->ptr = new PtrRegister(PtrUnion(0));
}

Processor::~Processor() {
    delete this->r1;
    delete this->r2;
    delete this->rr;

    delete this->ic;
    delete this->c;
    delete this->ss;
    delete this->cf;
    delete this->ti;
    delete this->si;
    delete this->ptr;
}

void Processor::handleInterrupts() {
    auto si = this->getSI()->getValue();
    if (si == SI_REGISTER_VALUE_PB || si == SI_REGISTER_VALUE_GB) {
        this->inst_exchange();
        this->getSI()->setValue(0);
    }
}

uint8_t Processor::tick() {

    //Increases IC inside.
    auto instruction = this->readNextInstructionFromMemory();
    auto result = this->runInstruction(instruction);
    this->handleInterrupts();

    return result;

}

uint8_t Processor::runInstruction(memory_word_t instruction) {
    auto instructionExpanded = reinterpret_cast<uint8_t *>(&instruction);
    uint8_t instructionType = instructionExpanded[3];

    switch (instructionType) {
        case INSTRUCTION_TYPE_ADD:
            this->inst_add(instructionExpanded[2], instructionExpanded[1]);
            break;
        case INSTRUCTION_TYPE_SUB:
            this->inst_sub(instructionExpanded[2], instructionExpanded[1]);
            break;
        case INSTRUCTION_TYPE_MUL:
            this->inst_mul(instructionExpanded[2], instructionExpanded[1]);
            break;
        case INSTRUCTION_TYPE_DIV:
            this->inst_div(instructionExpanded[2], instructionExpanded[1]);
            break;
        case INSTRUCTION_TYPE_NEG:
            this->inst_neg(instructionExpanded[2]);
            break;
        case INSTRUCTION_TYPE_INC:
            this->inst_inc(instructionExpanded[2]);
            break;
        case INSTRUCTION_TYPE_DEC:
            this->inst_dec(instructionExpanded[2]);
            break;
        case INSTRUCTION_TYPE_AND:
            this->inst_and(instructionExpanded[2], instructionExpanded[1]);
            break;
        case INSTRUCTION_TYPE_OR:
            this->inst_or(instructionExpanded[2], instructionExpanded[1]);
            break;
        case INSTRUCTION_TYPE_NOT:
            this->inst_not(instructionExpanded[2]);
            break;
        case INSTRUCTION_TYPE_LSHIFT:
            this->inst_lshift(instructionExpanded[2], instructionExpanded[1]);
            break;
        case INSTRUCTION_TYPE_RSHIFT:
            this->inst_rshift(instructionExpanded[2], instructionExpanded[1]);
            break;
        case INSTRUCTION_TYPE_CMP:
            this->inst_cmp(instructionExpanded[2], instructionExpanded[1]);
            break;
        case INSTRUCTION_TYPE_JM:
            this->inst_jm(instructionExpanded[2], instructionExpanded[1]);
            break;
        case INSTRUCTION_TYPE_JL:
            this->inst_jl(instructionExpanded[2], instructionExpanded[1]);
            break;
        case INSTRUCTION_TYPE_JE:
            this->inst_je(instructionExpanded[2], instructionExpanded[1]);
            break;
        case INSTRUCTION_TYPE_JG:
            this->inst_jg(instructionExpanded[2], instructionExpanded[1]);
            break;
        case INSTRUCTION_TYPE_CALL:
            this->inst_call(instructionExpanded[2], instructionExpanded[1]);
            break;
        case INSTRUCTION_TYPE_RET:
            this->inst_ret();
            break;
        case INSTRUCTION_TYPE_LOOP:
            this->inst_loop(instructionExpanded[2], instructionExpanded[1]);
            break;
        case INSTRUCTION_TYPE_HALT:
            this->inst_halt();
            break;
        case INSTRUCTION_TYPE_PB:
            this->inst_pb(instructionExpanded[2]);
            break;
        case INSTRUCTION_TYPE_GB:
            this->inst_gb(instructionExpanded[2]);
            break;
        case INSTRUCTION_TYPE_SSR:
            this->inst_ssr(instructionExpanded[2]);
            break;
        case INSTRUCTION_TYPE_LSR:
            this->inst_lsr(instructionExpanded[2]);
            break;
        case INSTRUCTION_TYPE_LW_REG_REG:
            this->inst_lwrr(instructionExpanded[2], instructionExpanded[1]);
            break;
        case INSTRUCTION_TYPE_LW_REG_CONST:
            this->inst_lwrc(instructionExpanded[2], this->readNextInstructionFromMemory());
            break;
        case INSTRUCTION_TYPE_LW_REG_MEM:
            this->inst_lwrm(instructionExpanded[2], instructionExpanded[1], instructionExpanded[0]);
            break;
        case INSTRUCTION_TYPE_LW_MEM_REG:
            this->inst_lwmr(instructionExpanded[2], instructionExpanded[1], instructionExpanded[0]);
            break;
        default:
            std::cout << "[FATAL] Unrecognized instruction... Halting..." << std::endl;
            this->inst_halt();
            break;
    }

    return instructionType;
}

/**
 * Arithmetic instructions
*/

void Processor::inst_add(register_index_t firstRegisterIndex, register_index_t secondRegisterIndex) {
    if (firstRegisterIndex > REGISTER_INDEX_MAX || firstRegisterIndex < REGISTER_INDEX_MIN)
        throw std::invalid_argument("Processor::inst_add firstRegisterIndex is out of bounds!");

    if (secondRegisterIndex > REGISTER_INDEX_MAX || secondRegisterIndex < REGISTER_INDEX_MIN)
        throw std::invalid_argument("Processor::inst_add secondRegisterIndex is out of bounds!");

    auto first = this->getWordFromRegister(firstRegisterIndex);
    auto second = this->getWordFromRegister(secondRegisterIndex);
    this->writeWordIntoRegister(firstRegisterIndex, first + second);
}

void Processor::inst_sub(register_index_t firstRegisterIndex, register_index_t secondRegisterIndex) {
    if (firstRegisterIndex > REGISTER_INDEX_MAX || firstRegisterIndex < REGISTER_INDEX_MIN)
        throw std::invalid_argument("Processor::inst_sub firstRegisterIndex is out of bounds!");

    if (secondRegisterIndex > REGISTER_INDEX_MAX || secondRegisterIndex < REGISTER_INDEX_MIN)
        throw std::invalid_argument("Processor::inst_sub secondRegisterIndex is out of bounds!");

    auto first = this->getWordFromRegister(firstRegisterIndex);
    auto second = this->getWordFromRegister(secondRegisterIndex);
    this->writeWordIntoRegister(firstRegisterIndex, first - second);
}

void Processor::inst_mul(register_index_t firstRegisterIndex, register_index_t secondRegisterIndex) {
    if (firstRegisterIndex > REGISTER_INDEX_MAX || firstRegisterIndex < REGISTER_INDEX_MIN)
        throw std::invalid_argument("Processor::inst_mul firstRegisterIndex is out of bounds!");

    if (secondRegisterIndex > REGISTER_INDEX_MAX || secondRegisterIndex < REGISTER_INDEX_MIN)
        throw std::invalid_argument("Processor::inst_mul secondRegisterIndex is out of bounds!");

    auto first = this->getWordFromRegister(firstRegisterIndex);
    auto second = this->getWordFromRegister(secondRegisterIndex);
    this->writeWordIntoRegister(firstRegisterIndex, first * second);
}

void Processor::inst_div(register_index_t firstRegisterIndex, register_index_t secondRegisterIndex) {
    if (firstRegisterIndex > REGISTER_INDEX_MAX || firstRegisterIndex < REGISTER_INDEX_MIN)
        throw std::invalid_argument("Processor::inst_div firstRegisterIndex is out of bounds!");

    if (secondRegisterIndex > REGISTER_INDEX_MAX || secondRegisterIndex < REGISTER_INDEX_MIN)
        throw std::invalid_argument("Processor::inst_div secondRegisterIndex is out of bounds!");

    auto first = this->getWordFromRegister(firstRegisterIndex);
    auto second = this->getWordFromRegister(secondRegisterIndex);
    this->writeWordIntoRegister(firstRegisterIndex, first / second);
    this->getRR()->setValue(first % second);
}

void Processor::inst_neg(register_index_t targetRegisterIndex) {
    if (targetRegisterIndex > REGISTER_INDEX_MAX || targetRegisterIndex < REGISTER_INDEX_MIN)
        throw std::invalid_argument("Processor::inst_neg targetRegisterIndex is out of bounds!");

    auto value = this->getWordFromRegister(targetRegisterIndex);
    this->writeWordIntoRegister(targetRegisterIndex, value * -1);
}

void Processor::inst_inc(register_index_t targetRegisterIndex) {
    if (targetRegisterIndex > REGISTER_INDEX_MAX || targetRegisterIndex < REGISTER_INDEX_MIN)
        throw std::invalid_argument("Processor::inst_inc targetRegisterIndex is out of bounds!");

    auto value = this->getWordFromRegister(targetRegisterIndex);
    this->writeWordIntoRegister(targetRegisterIndex, value + 1);
}

void Processor::inst_dec(register_index_t targetRegisterIndex) {
    if (targetRegisterIndex > REGISTER_INDEX_MAX || targetRegisterIndex < REGISTER_INDEX_MIN)
        throw std::invalid_argument("Processor::inst_dec targetRegisterIndex is out of bounds!");

    auto value = this->getWordFromRegister(targetRegisterIndex);
    this->writeWordIntoRegister(targetRegisterIndex, value - 1);
}

/**
 * Logical instructions
 */
void Processor::inst_and(register_index_t firstRegisterIndex, register_index_t secondRegisterIndex) {
    if (firstRegisterIndex > REGISTER_INDEX_MAX || firstRegisterIndex < REGISTER_INDEX_MIN)
        throw std::invalid_argument("Processor::inst_add firstRegisterIndex is out of bounds!");

    if (secondRegisterIndex > REGISTER_INDEX_MAX || secondRegisterIndex < REGISTER_INDEX_MIN)
        throw std::invalid_argument("Processor::inst_add secondRegisterIndex is out of bounds!");

    auto first = this->getWordFromRegister(firstRegisterIndex);
    auto second = this->getWordFromRegister(secondRegisterIndex);
    this->writeWordIntoRegister(firstRegisterIndex, first & second);
}

void Processor::inst_or(register_index_t firstRegisterIndex, register_index_t secondRegisterIndex) {
    if (firstRegisterIndex > REGISTER_INDEX_MAX || firstRegisterIndex < REGISTER_INDEX_MIN)
        throw std::invalid_argument("Processor::inst_add firstRegisterIndex is out of bounds!");

    if (secondRegisterIndex > REGISTER_INDEX_MAX || secondRegisterIndex < REGISTER_INDEX_MIN)
        throw std::invalid_argument("Processor::inst_add secondRegisterIndex is out of bounds!");

    auto first = this->getWordFromRegister(firstRegisterIndex);
    auto second = this->getWordFromRegister(secondRegisterIndex);
    this->writeWordIntoRegister(firstRegisterIndex, first | second);
}

void Processor::inst_not(register_index_t targetRegisterIndex) {
    if (targetRegisterIndex > REGISTER_INDEX_MAX || targetRegisterIndex < REGISTER_INDEX_MIN)
        throw std::invalid_argument("Processor::inst_inc targetRegisterIndex is out of bounds!");

    auto value = this->getWordFromRegister(targetRegisterIndex);
    this->writeWordIntoRegister(targetRegisterIndex, ~value);
}

void Processor::inst_lshift(register_index_t targetRegisterIndex, uint8_t data) {
    if (targetRegisterIndex > REGISTER_INDEX_MAX || targetRegisterIndex < REGISTER_INDEX_MIN)
        throw std::invalid_argument("Processor::inst_inc targetRegisterIndex is out of bounds!");

    auto value = this->getWordFromRegister(targetRegisterIndex);
    this->writeWordIntoRegister(targetRegisterIndex, value << data);
}

void Processor::inst_rshift(register_index_t targetRegisterIndex, uint8_t data) {
    if (targetRegisterIndex > REGISTER_INDEX_MAX || targetRegisterIndex < REGISTER_INDEX_MIN)
        throw std::invalid_argument("Processor::inst_inc targetRegisterIndex is out of bounds!");

    auto value = this->getWordFromRegister(targetRegisterIndex);
    this->writeWordIntoRegister(targetRegisterIndex, value >> data);
}

/**
 * Comparison instruction
 */

void Processor::inst_cmp(register_index_t firstRegisterIndex, register_index_t secondRegisterIndex) {
    if (firstRegisterIndex > REGISTER_INDEX_MAX || firstRegisterIndex < REGISTER_INDEX_MIN)
        throw std::invalid_argument("Processor::inst_add firstRegisterIndex is out of bounds!");

    if (secondRegisterIndex > REGISTER_INDEX_MAX || secondRegisterIndex < REGISTER_INDEX_MIN)
        throw std::invalid_argument("Processor::inst_add secondRegisterIndex is out of bounds!");

    auto first = this->getWordFromRegister(firstRegisterIndex);
    auto second = this->getWordFromRegister(secondRegisterIndex);
    auto *reg = this->getCF();

    if (first > second)
        reg->setValue(0);
    else if (first == second)
        reg->setValue(1);
    else
        reg->setValue(2);
}

/**
 * Jump instructions
 */

void Processor::inst_jm(uint8_t firstByte, uint8_t secondByte) {
    auto argument = secondByte + 256U * firstByte;
    this->getIC()->setValue(argument);
}

void Processor::inst_jl(uint8_t firstByte, uint8_t secondByte) {
    if (this->getCF()->getValue() != 2)
        return;

    this->inst_jm(firstByte, secondByte);
}

void Processor::inst_je(uint8_t firstByte, uint8_t secondByte) {
    if (this->getCF()->getValue() != 1)
        return;

    this->inst_jm(firstByte, secondByte);
}

void Processor::inst_jg(uint8_t firstByte, uint8_t secondByte) {
    if (this->getCF()->getValue() != 0)
        return;

    this->inst_jm(firstByte, secondByte);
}

void Processor::inst_call(uint8_t firstByte, uint8_t secondByte) {
    this->inst_ssr(REGISTER_INDEX_IC);
    this->inst_jm(firstByte, secondByte);
}

void Processor::inst_ret() {
    this->inst_lsr(REGISTER_INDEX_IC);
    this->inst_inc(REGISTER_INDEX_IC);
}

void Processor::inst_loop(uint8_t firstByte, uint8_t secondByte) {
    if (this->getC()->getValue() > 0) {
        auto argument = firstByte + 256U * secondByte;
        this->getIC()->setValue(argument);
        this->inst_dec(REGISTER_INDEX_C);
    }
}

void Processor::inst_halt() {
    cout << "[INFO] Machine halted in user mode..." << endl;
    this->getSI()->setValue(SI_REGISTER_VALUE_HALT);
}

void Processor::inst_pb(uint8_t blockAddress) {
    if (blockAddress >= VIRTUAL_DATA_SEGMENT_SIZE)
        throw std::invalid_argument("PB instruction in virtual mode was called with invalid block address!");

    auto channel = this->getMachine()->getChannelManager();

    channel->getDb()->setValue(CHANNEL_OBJECT_INPUT_OUTPUT);
    channel->getDt()->setValue(0);

    auto realBlockAddress = this->getMachine()->getPageMechanism()->getRealBlockFromVirtualBlock(
            this->getPTR()->getValue(),
            blockAddress
    );

    channel->getSb()->setValue(CHANNEL_OBJECT_USER_MEMORY);
    channel->getSt()->setValue(realBlockAddress);
    this->getSI()->setValue(SI_REGISTER_VALUE_PB);
}

void Processor::inst_gb(uint8_t blockAddress) {
    if (blockAddress >= VIRTUAL_DATA_SEGMENT_SIZE)
        throw std::invalid_argument("GB instruction in virtual mode was called with invalid block address!");

    auto channel = this->getMachine()->getChannelManager();

    channel->getSb()->setValue(CHANNEL_OBJECT_INPUT_OUTPUT);
    channel->getSt()->setValue(0);

    auto realBlockAddress = this->getMachine()->getPageMechanism()->getRealBlockFromVirtualBlock(
            this->getPTR()->getValue(),
            blockAddress
    );

    channel->getDb()->setValue(CHANNEL_OBJECT_USER_MEMORY);
    channel->getDt()->setValue(realBlockAddress);
    this->getSI()->setValue(SI_REGISTER_VALUE_GB);
}

void Processor::inst_exchange() {
    auto channel = this->getMachine()->getChannelManager();
    channel->exchange(this);
}

void Processor::inst_ssr(register_index_t registerIndex) {
    auto stackPtr = static_cast<uint16_t>(this->getSS()->getValue());
    if (stackPtr >= VIRTUAL_STACK_SEGMENT_SIZE * MEMORY_BLOCK_SIZE)
        throw std::runtime_error("Processor::init_ssr was called when stack pointer has overflow!");

    auto memory = this->getMachine()->getMemory();
    auto word = this->getWordFromRegister(registerIndex);

    auto rstackBlockAddress = this->getMachine()->getPageMechanism()->getRealBlockFromVirtualBlock(
            this->getPTR()->getValue(),
            MEMORY_USER_BLOCK_OFFSET + stackPtr / MEMORY_BLOCK_SIZE
    );

    memory->getUserMemory()->writeWord(
            rstackBlockAddress,
            stackPtr % MEMORY_BLOCK_SIZE,
            word
    );

    this->getSS()->setValue(stackPtr + 1);
}

void Processor::inst_lsr(uint8_t registerIndex) {
    auto stackPtr = static_cast<uint16_t>(this->getSS()->getValue());
    if (stackPtr >= VIRTUAL_STACK_SEGMENT_SIZE * MEMORY_BLOCK_SIZE || stackPtr <= 0)
        throw std::runtime_error("Processor::init_lsr was called when stack pointer has overflow!");

    stackPtr = stackPtr - 1;
    auto memory = this->getMachine()->getMemory();
    auto rstackBlockAddress = this->getMachine()->getPageMechanism()->getRealBlockFromVirtualBlock(
            this->getPTR()->getValue(),
            MEMORY_USER_BLOCK_OFFSET + stackPtr / MEMORY_BLOCK_SIZE
    );

    memory_word_t word = memory->getUserMemory()->readWord(
            rstackBlockAddress,
            stackPtr % MEMORY_BLOCK_SIZE
    );
    this->writeWordIntoRegister(registerIndex, word);

    this->getSS()->setValue(stackPtr);
}

void Processor::inst_lwrr(uint8_t sourceRegisterIndex, uint8_t destRegisterIndex) {
    auto data = this->getWordFromRegister(sourceRegisterIndex);
    this->writeWordIntoRegister(destRegisterIndex, data);
}

void Processor::inst_lwrc(uint8_t destRegisterIndex, memory_word_t data) {
    this->writeWordIntoRegister(destRegisterIndex, data);
}

void Processor::inst_lwrm(register_index_t registerIndex, block_address_t blockAddress, word_address_t wordAddress) {
    auto memory = this->getMachine()->getMemory();
    auto data = this->getWordFromRegister(registerIndex);

    auto rBlockAddress = this->getMachine()->getPageMechanism()->getRealBlockFromVirtualBlock(
            this->getPTR()->getValue(),
            blockAddress + VIRTUAL_DATA_SEGMENT_OFFSET
    );

    memory->getUserMemory()->writeWord(rBlockAddress, wordAddress, data);
}

void Processor::inst_lwmr(block_address_t blockAddress, word_address_t wordAddress, register_index_t registerIndex) {
    auto memory = this->getMachine()->getMemory();

    auto rBlockAddress = this->getMachine()->getPageMechanism()->getRealBlockFromVirtualBlock(
            this->getPTR()->getValue(),
            blockAddress + VIRTUAL_DATA_SEGMENT_OFFSET
    );

    memory_word_t data = memory->getUserMemory()->readWord(rBlockAddress, wordAddress);

    this->writeWordIntoRegister(registerIndex, data);
}

Int32Register *Processor::getMemoryRegisterFromIndex(register_index_t idx) const {
    switch (idx) {
        case REGISTER_INDEX_R1:
            return this->getR1();
        case REGISTER_INDEX_R2:
            return this->getR2();
        case REGISTER_INDEX_RR:
            return this->getRR();
        default:
            throw std::invalid_argument(
                    "Processor::getMemoryRegisterFromIndex argument register_index_t idx is out of bounds, passed (" +
                    std::to_string(idx) + ")"
            );
    }
}

memory_word_t Processor::getWordFromRegister(register_index_t idx) const {
    switch (idx) {
        case REGISTER_INDEX_R1:
            return this->getR1()->getValue();
        case REGISTER_INDEX_R2:
            return this->getR2()->getValue();
        case REGISTER_INDEX_RR:
            return this->getRR()->getValue();
        case REGISTER_INDEX_IC:
            return this->getIC()->getValue();
        case REGISTER_INDEX_C:
            return this->getC()->getValue();
        case REGISTER_INDEX_SS:
            return this->getSS()->getValue();
        case REGISTER_INDEX_CF:
            return this->getCF()->getValue();
        case REGISTER_INDEX_TI:
            return this->getTI()->getValue();
        case REGISTER_INDEX_SI:
            return this->getSI()->getValue();
        case REGISTER_INDEX_PTR:
            return this->getPTR()->getValue().getValue();
        default:
            throw std::invalid_argument(
                    "Processor::getWordFromRegister argument register_index_t idx is out of bounds, passed (" +
                    std::to_string(idx) + ")"
            );
    }
}

void Processor::writeWordIntoRegister(register_index_t idx, memory_word_t word) const {
    switch (idx) {
        case REGISTER_INDEX_R1:
            this->getR1()->setValue(word);
            break;
        case REGISTER_INDEX_R2:
            this->getR2()->setValue(word);
            break;
        case REGISTER_INDEX_RR:
            this->getRR()->setValue(word);
            break;
        case REGISTER_INDEX_IC:
            this->getIC()->setValue(word);
            break;
        case REGISTER_INDEX_C:
            this->getC()->setValue(word);
            break;
        case REGISTER_INDEX_SS:
            this->getSS()->setValue(word);
            break;
        case REGISTER_INDEX_CF:
            this->getCF()->setValue(word);
            break;
        case REGISTER_INDEX_TI:
            this->getTI()->setValue(word);
            break;
        case REGISTER_INDEX_SI:
            this->getSI()->setValue(word);
            break;
        case REGISTER_INDEX_PTR:
            this->getPTR()->setValue(PtrUnion(word));
            break;
        default:
            throw std::invalid_argument(
                    "Processor::getWordFromRegister argument register_index_t idx is out of bounds, passed (" +
                    std::to_string(idx) + ")"
            );
    }
}

memory_word_t Processor::readNextInstructionFromMemory() const {
    auto memory = this->getMachine()->getMemory();

    auto icval = this->getIC()->getValue();
    this->getIC()->setValue(icval + 1);

    auto tival = this->getTI()->getValue();
    this->getTI()->setValue(tival - 1);

    if (icval >= VIRTUAL_CODE_SEGMENT_SIZE * MEMORY_BLOCK_SIZE)
        throw std::runtime_error("Processor IC value has grown outside code segment in user mode!");

    auto icrval = this->getMachine()->getPageMechanism()->getRealBlockFromVirtualBlock(
            this->getPTR()->getValue(),
            (icval / MEMORY_BLOCK_SIZE) + VIRTUAL_CODE_SEGMENT_OFFSET
    ) * MEMORY_BLOCK_SIZE + icval % MEMORY_BLOCK_SIZE;
    return memory->getUserMemory()->readWordCombined(icrval);
}

void Processor::print() {
    cout << hex
         << "R1: " << r1->getValue()
         << " IC: " << (uint32_t) ic->getValue()
         << " SS: " << (uint32_t) ss->getValue() << endl

         << "R2: " << r2->getValue()
         << " C: " << c->getValue()
         << " SI: " << (uint32_t) si->getValue() << endl

         << "RR: " << rr->getValue()
         << " CF: " << (uint32_t) cf->getValue()

         << " PTR: " << (uint32_t) ptr->getValue().getExpanded().unused
         << " " << (uint32_t) ptr->getValue().getExpanded().pageTableSize
         << " " << (uint32_t) ptr->getValue().getExpanded().realBlockAddressSize
         << " " << (uint32_t) ptr->getValue().getExpanded().realWordAddressSize

         << " TI: " << (uint32_t) ti->getValue() << endl;
}

Int32Register *Processor::getR1() const {
    return r1;
}

Int32Register *Processor::getR2() const {
    return r2;
}

Int32Register *Processor::getRR() const {
    return rr;
}

UInt16Register *Processor::getIC() const {
    return ic;
}

UInt32Register *Processor::getC() const {
    return c;
}

UInt8Register *Processor::getSS() const {
    return ss;
}

UInt8Register *Processor::getCF() const {
    return cf;
}

UInt8Register *Processor::getTI() const {
    return ti;
}

UInt8Register *Processor::getSI() const {
    return si;
}

PtrRegister *Processor::getPTR() const {
    return ptr;
}

RealMachine *Processor::getMachine() const {
    return machine;
}
