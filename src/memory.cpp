//
// Created by kudze on 2022-03-24.
//

#include <iostream>
#include <iomanip>
#include <cmath>
#include "memory.h"

void MemoryInterface::writeBlockString(block_address_t blockAddress, const std::string &message) {
    auto message_len = message.length();
    if (message_len >= 64)
        throw std::invalid_argument("writeStringToMemoryBlock message length should be less than 64");

    auto msg_ptr = reinterpret_cast<const memory_word_t *>(message.c_str());
    auto wordsToWrite = (word_address_t) ceil(message_len / 4.f);

    for (word_address_t i = 0; i < wordsToWrite; i++)
        this->writeWord(blockAddress, i, msg_ptr[i]);

    for (word_address_t i = wordsToWrite; i < MEMORY_BLOCK_SIZE; i++)
        this->writeWord(blockAddress, i, 0x00000000);
}

bool Memory::isMemoryBlockZeroed(memory_block_t block) {
    for (word_address_t j = 0; j < MEMORY_BLOCK_SIZE; j++)
        if (block[j] != 0)
            return false;

    return true;
}

Memory::Memory()
        : userMemory(new UserMemory(this)), supervisorMemory(new SupervisorMemory(this)) {

}

Memory::~Memory() {
    delete this->userMemory;
    delete this->supervisorMemory;
}

memory_block_t Memory::readBlock(block_address_t blockAddress) const {
    return this->memory.at(blockAddress);
}

memory_word_t Memory::readWord(block_address_t blockAddress, word_address_t wordAddress) const {
    return this->memory.at(blockAddress).at(wordAddress);
}

memory_word_t MemoryInterface::readWordCombined(uint16_t combinedAddress) const {
    return this->readWord(combinedAddress / MEMORY_BLOCK_SIZE, combinedAddress % MEMORY_BLOCK_SIZE);
}

void Memory::writeWord(block_address_t blockAddress, word_address_t wordAddress, memory_word_t value) {
    this->memory.at(blockAddress).at(wordAddress) = value;
}

void MemoryInterface::writeWordCombined(uint16_t combinedAddress, memory_word_t value) {
    this->writeWord(combinedAddress / MEMORY_BLOCK_SIZE, combinedAddress % MEMORY_BLOCK_SIZE, value);
}

void Memory::writeBlock(block_address_t blockAddress, const memory_block_t &block) {
    this->memory.at(blockAddress) = block;
}

UserMemory *Memory::getUserMemory() const {
    return userMemory;
}

SupervisorMemory *Memory::getSupervisorMemory() const {
    return supervisorMemory;
}

SupervisorMemory::SupervisorMemory(Memory *memory)
        : memory(memory) {}

Memory *SupervisorMemory::getMemory() const {
    return memory;
}

void SupervisorMemory::writeBlock(block_address_t blockAddress, const memory_block_t &block) {
    memory->writeBlock(MEMORY_SUPERVISOR_BLOCK_OFFSET + blockAddress, block);
}

void SupervisorMemory::writeWord(block_address_t blockAddress, word_address_t wordAddress, memory_word_t value) {
    memory->writeWord(MEMORY_SUPERVISOR_BLOCK_OFFSET + blockAddress, wordAddress, value);
}

memory_word_t SupervisorMemory::readWord(block_address_t blockAddress, word_address_t wordAddress) const {
    return memory->readWord(MEMORY_SUPERVISOR_BLOCK_OFFSET + blockAddress, wordAddress);
}

memory_block_t SupervisorMemory::readBlock(block_address_t blockAddress) const {
    return memory->readBlock(MEMORY_SUPERVISOR_BLOCK_OFFSET + blockAddress);
}

void SupervisorMemory::print() const {
    std::cout << "Supervisor memory: " << std::endl << "  ";

    for (int i = 0; i < MEMORY_BLOCK_SIZE; i++) {
        std::cout << std::hex << std::setfill('0') << std::setw(2) << "    " << i << "    ";
    }
    std::cout << std::endl;
    for (int i = 0; i < MEMORY_SUPERVISOR_BLOCK_COUNT; i++) {
        std::cout << std::hex << std::setfill('0') << std::setw(2) << i << " ";
        for (int j = 0; j < MEMORY_BLOCK_SIZE; ++j) {
            std::cout << std::hex << std::setfill('0') << std::setw(8) << readWord(i, j) << " ";
        }
        std::cout << std::endl;
    }
}

UserMemory::UserMemory(Memory *memory)
        : memory(memory) {}

Memory *UserMemory::getMemory() const {
    return memory;
}

void UserMemory::writeBlock(block_address_t blockAddress, const memory_block_t &block) {
    memory->writeBlock(MEMORY_USER_BLOCK_OFFSET + blockAddress, block);
}

void UserMemory::writeWord(block_address_t blockAddress, word_address_t wordAddress, memory_word_t value) {
    memory->writeWord(MEMORY_USER_BLOCK_OFFSET + blockAddress, wordAddress, value);
}

memory_word_t UserMemory::readWord(block_address_t blockAddress, word_address_t wordAddress) const {
    return memory->readWord(MEMORY_USER_BLOCK_OFFSET + blockAddress, wordAddress);
}

memory_block_t UserMemory::readBlock(block_address_t blockAddress) const {
    return memory->readBlock(MEMORY_USER_BLOCK_OFFSET + blockAddress);
}

void UserMemory::print() const {
    std::cout << "User memory: " << std::endl << "  ";

    for (int i = 0; i < MEMORY_BLOCK_SIZE; i++) {
        std::cout << std::hex << std::setfill('0') << std::setw(2) << "    " << i << "    ";
    }
    std::cout << std::endl;
    for (int i = 0; i < MEMORY_USER_BLOCK_COUNT; i++) {
        std::cout << std::hex << std::setfill('0') << std::setw(2) << i << " ";
        for (int j = 0; j < MEMORY_BLOCK_SIZE; ++j) {
            std::cout << std::hex << std::setfill('0') << std::setw(8) << readWord(i, j) << " ";
        }
        std::cout << std::endl;
    }
}
