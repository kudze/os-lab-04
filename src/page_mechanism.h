//
// Created by kudze on 2022-03-27.
//

#ifndef LAB04_PAGE_MECHANISM_H
#define LAB04_PAGE_MECHANISM_H

#include <cstdint>
#include <stdexcept>

typedef uint8_t vm_index_t;
#define MAX_VMS 16

#include "memory.h"
#include "registers.h"

class PageMechanism {
    Memory* memory;

    //This just initializes page segment on supervisor memory to be all zeros.
    //We will use this logic to detect if page table is used or not.
    void initPageSegmentMemory();

public:
    explicit PageMechanism(Memory* memory);
    ~PageMechanism();

    PtrUnion allocateVirtualMachineMemory() const;
    PtrUnion getFirstFreePageTableAddress() const;
    PtrUnion getNthPageTableAddress(vm_index_t index) const; //Retuns ptr value for nth page table. Doesn't check if allocated
    bool isPageTableAddressUsed(PtrUnion index) const;
    bool isUserBlockUsed(block_address_t blockAddress) const;

    block_address_t getRealBlockFromVirtualBlock(PtrUnion const& ptr, block_address_t virtualBlockAddress) const;

    Memory *getMemory() const;
};


#endif //LAB04_PAGE_MECHANISM_H
