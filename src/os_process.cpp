//
// Created by kudze on 2022-05-29.
//

#include "os_process.h"

#include <iostream>
#include <cstring>
#include <sstream>

using namespace std;


BootProcess::BootProcess(ProcessManager *manager)
        : Process(manager) {}

void BootProcess::createSystemProcesses() {
    cout << "BootProcess is initializing processes..." << endl;

    auto manager = this->getManager();

    manager->registerProcess(new InstallProcess(manager), 250, "Install", PROCESS_INSTALL_ID, this->getId());
    manager->registerProcess(new InterruptService(manager), 210, "Interrupt", PROCESS_INTERRUPT_ID, this->getId());
    manager->registerProcess(new MainProcess(manager), 200, "Main", PROCESS_MAIN_ID, this->getId());
    manager->registerProcess(new TerminalProcess(manager), 5, "Terminal", PROCESS_TERMINAL_ID, this->getId());
    manager->registerProcess(new ShutdownProcess(manager), 0, "Shutdown", PROCESS_SHUTDOWN_ID, this->getId());
}

void BootProcess::createStaticResources() {
    cout << "BootProcess is initializing resources..." << endl;

    auto resourceManager = this->getResourceManager();

    resourceManager->registerResource(this->getId(), RESOURCE_MACHINE_STOP_ID);
    resourceManager->registerResource(this->getId(), RESOURCE_INSTALL_ID);

    resourceManager->registerResource(this->getId(), RESOURCE_CHANNEL_REGISTER_ID);
    resourceManager->releaseResourceElement(this, RESOURCE_CHANNEL_REGISTER_ID, 0);

    resourceManager->registerResource(this->getId(), RESOURCE_MEMORY_TASK_ID);
    resourceManager->registerResource(this->getId(), RESOURCE_JG_LOAD_TASK_ID);
    resourceManager->registerResource(this->getId(), RESOURCE_JG_INTERRUPT_ID);
    resourceManager->registerResource(this->getId(), RESOURCE_INTERRUPT_ID);
}

void BootProcess::destroyStaticResources() {
    cout << "BootProcess is destroying resources..." << endl;

    auto resourceManager = this->getResourceManager();

    resourceManager->unregisterResource(RESOURCE_MACHINE_STOP_ID);
    resourceManager->unregisterResource(RESOURCE_INSTALL_ID);
    resourceManager->unregisterResource(RESOURCE_CHANNEL_REGISTER_ID);
    resourceManager->unregisterResource(RESOURCE_MEMORY_TASK_ID);
    resourceManager->unregisterResource(RESOURCE_JG_LOAD_TASK_ID);
    resourceManager->unregisterResource(RESOURCE_JG_INTERRUPT_ID);
    resourceManager->unregisterResource(RESOURCE_INTERRUPT_ID);
}

void BootProcess::destroySystemProcesses() {
    cout << "BootProcess is destroying processes..." << endl;

    auto manager = this->getManager();

    manager->unregisterProcess(PROCESS_INSTALL_ID);
    manager->unregisterProcess(PROCESS_MAIN_ID);
    manager->unregisterProcess(PROCESS_SHUTDOWN_ID);
    manager->unregisterProcess(PROCESS_TERMINAL_ID);
    manager->unregisterProcess(PROCESS_INTERRUPT_ID);
}

void BootProcess::run() {
    if (this->shutdown) {
        this->destroySystemProcesses();
        this->destroyStaticResources();

        this->getManager()->getMachine()->stopOnNextTick();

        return;
    }

    this->createStaticResources();
    this->createSystemProcesses();

    auto resourceManager = this->getManager()->getMachine()->getResourceManager();
    resourceManager->requestResourceElement(this, RESOURCE_MACHINE_STOP_ID);
}

void BootProcess::acceptResourceElement(resource_id_t resourceId, uint8_t data, process_id_t senderId) {
    if (resourceId == RESOURCE_MACHINE_STOP_ID)
        this->shutdown = true;
}


ShutdownProcess::ShutdownProcess(ProcessManager *manager)
        : Process(manager) {}

void ShutdownProcess::run() {
    auto resourceManager = this->getResourceManager();
    resourceManager->releaseResourceElement(this, RESOURCE_MACHINE_STOP_ID, 0);
}


InstallProcess::InstallProcess(ProcessManager *manager)
        : Process(manager), installToSlot(0xFF), hasChannelRegister(false) {

}

void InstallProcess::run() {
    auto resourceManager = this->getResourceManager();

    if (this->installToSlot == 0xFF) {
        resourceManager->requestResourceElement(this, RESOURCE_INSTALL_ID);
        return;
    }

    if (!this->hasChannelRegister) {
        resourceManager->requestResourceElement(this, RESOURCE_CHANNEL_REGISTER_ID);
        return;
    }

    auto manager = this->getManager();
    auto machine = manager->getMachine();
    auto memory = machine->getMemory();
    auto supervisor = memory->getSupervisorMemory();
    auto channelManager = machine->getChannelManager();
    auto processor = machine->getProcessor();

    //Lets load strings we will need.
    supervisor->writeBlockString(SUPERVISOR_UNDEFINED_SEGMENT_OFFSET, "Įveskite programos blokus!");
    supervisor->writeBlockString(SUPERVISOR_UNDEFINED_SEGMENT_OFFSET + 1, "Antraštės blokas yra neteisingas!");
    supervisor->writeBlockString(SUPERVISOR_UNDEFINED_SEGMENT_OFFSET + 2, "Programa irašyta!");

    //Lets tell user what to do.
    channelManager->getSb()->setValue(CHANNEL_OBJECT_SUPERVISOR_MEMORY);
    channelManager->getSt()->setValue(SUPERVISOR_UNDEFINED_SEGMENT_OFFSET);
    channelManager->getDb()->setValue(CHANNEL_OBJECT_INPUT_OUTPUT);
    channelManager->getDt()->setValue(0);
    channelManager->exchange(processor);

    //Lets read first block off input.
    channelManager->getSb()->setValue(CHANNEL_OBJECT_INPUT_OUTPUT_HEX);
    channelManager->getSt()->setValue(0);
    channelManager->getDb()->setValue(CHANNEL_OBJECT_SUPERVISOR_MEMORY);
    channelManager->getDt()->setValue(SUPERVISOR_UNDEFINED_SEGMENT_OFFSET + 3);
    channelManager->exchange(processor);

    auto headerBlock = supervisor->readBlock(SUPERVISOR_UNDEFINED_SEGMENT_OFFSET + 3);
    if (!InstallProcess::isHeaderValid(headerBlock)) {
        //Lets tell user header is malformed.
        channelManager->getSb()->setValue(CHANNEL_OBJECT_SUPERVISOR_MEMORY);
        channelManager->getSt()->setValue(SUPERVISOR_UNDEFINED_SEGMENT_OFFSET + 1);
        channelManager->getDb()->setValue(CHANNEL_OBJECT_INPUT_OUTPUT);
        channelManager->getDt()->setValue(0);
        channelManager->exchange(processor);

        resourceManager->releaseResourceElement(this, RESOURCE_CHANNEL_REGISTER_ID, 0);
        this->installToSlot = 0xFF;
        return;
    }

    //otherwise lets write the program.
    channelManager->getSb()->setValue(CHANNEL_OBJECT_SUPERVISOR_MEMORY);
    channelManager->getSt()->setValue(SUPERVISOR_UNDEFINED_SEGMENT_OFFSET + 3);
    channelManager->getDb()->setValue(CHANNEL_OBJECT_OUTSIDE_MEMORY);
    channelManager->getDt()->setValue(this->installToSlot * EXTERNAL_MEMORY_TASK_SIZE);
    channelManager->exchange(processor);

    for (block_address_t i = 0; i < 10; i++) {
        channelManager->getSb()->setValue(CHANNEL_OBJECT_INPUT_OUTPUT_HEX);
        channelManager->getSt()->setValue(0);
        channelManager->getDb()->setValue(CHANNEL_OBJECT_OUTSIDE_MEMORY);
        channelManager->getDt()->setValue((this->installToSlot * EXTERNAL_MEMORY_TASK_SIZE) + 1 + i);
        channelManager->exchange(processor);
    }

    //Lets report success.
    channelManager->getSb()->setValue(CHANNEL_OBJECT_SUPERVISOR_MEMORY);
    channelManager->getSt()->setValue(SUPERVISOR_UNDEFINED_SEGMENT_OFFSET + 2);
    channelManager->getDb()->setValue(CHANNEL_OBJECT_INPUT_OUTPUT);
    channelManager->getDt()->setValue(0);
    channelManager->exchange(processor);

    resourceManager->releaseResourceElement(this, RESOURCE_CHANNEL_REGISTER_ID, 0);
    this->installToSlot = 0xFF;
}

void InstallProcess::acceptResourceElement(resource_id_t resourceId, uint8_t data, process_id_t senderId) {
    switch (resourceId) {
        case RESOURCE_INSTALL_ID:
            if (data > MAX_INSTALLABLE_SLOTS)
                return;

            this->installToSlot = data;
            break;
        case RESOURCE_CHANNEL_REGISTER_ID:
            this->hasChannelRegister = true;
            break;
    }
}

bool InstallProcess::isHeaderValid(memory_block_t header) {
    for (block_address_t i = 3; i < MEMORY_BLOCK_SIZE; i++)
        if (header[i] != 0x89ABCDEF)
            return false;

    return true;
}


MainProcess::MainProcess(ProcessManager *manager)
        : Process(manager), slot(0xFF), sender(0xFF) {


}

void MainProcess::run() {
    if (slot == 0xFF && sender == 0xFF) {
        this->getResourceManager()->requestResourceElement(this, RESOURCE_MEMORY_TASK_ID);
        return;
    }

    if (slot == 0xFF) {
        this->getManager()->unregisterProcess(sender);

        this->slot = 0xFF;
        this->sender = 0xFF;
        this->getResourceManager()->requestResourceElement(this, RESOURCE_MEMORY_TASK_ID);
        return;
    }

    auto jgtitle = std::string("JG ") + std::to_string((int) slot);
    auto jgid = this->getManager()->registerProcess(new JobGovernorProcess(this->getManager()), 190, jgtitle.c_str(),
                                                    0xFF, this->getId());
    this->getResourceManager()->releaseResourceElement(this, RESOURCE_JG_LOAD_TASK_ID, slot, jgid);

    this->slot = 0xFF;
    this->sender = 0xFF;
    this->getResourceManager()->requestResourceElement(this, RESOURCE_MEMORY_TASK_ID);
}

void MainProcess::acceptResourceElement(resource_id_t resourceId, uint8_t data, process_id_t senderId) {
    if (resourceId == RESOURCE_MEMORY_TASK_ID) {
        this->slot = data;
        this->sender = senderId;
    }
}


JobGovernorProcess::JobGovernorProcess(ProcessManager *manager)
        : Process(manager), hasChannelManager(false), slot(0xFF), vmid(0xFF) {

}

void JobGovernorProcess::printLine(const std::string &line) {
    auto manager = this->getManager();
    auto machine = manager->getMachine();
    auto memory = machine->getMemory();
    auto supervisor = memory->getSupervisorMemory();
    auto channelManager = machine->getChannelManager();
    auto processor = machine->getProcessor();

    supervisor->writeBlockString(SUPERVISOR_UNDEFINED_SEGMENT_OFFSET, line);
    channelManager->getSb()->setValue(CHANNEL_OBJECT_SUPERVISOR_MEMORY);
    channelManager->getSt()->setValue(SUPERVISOR_UNDEFINED_SEGMENT_OFFSET);
    channelManager->getDb()->setValue(CHANNEL_OBJECT_INPUT_OUTPUT);
    channelManager->getDt()->setValue(1);
    channelManager->exchange(processor);
}

bool JobGovernorProcess::boot() {
    if (this->vmid == 0xFF) {
        if (this->slot == 0xFF) {
            this->getResourceManager()->requestResourceElement(this, RESOURCE_JG_LOAD_TASK_ID);
            return false;
        }

        if (!this->hasChannelManager) {
            this->getResourceManager()->requestResourceElement(this, RESOURCE_CHANNEL_REGISTER_ID);
            return false;
        }

        auto manager = this->getManager();
        auto machine = manager->getMachine();
        auto memory = machine->getMemory();
        auto supervisor = memory->getSupervisorMemory();
        auto channelManager = machine->getChannelManager();
        auto pageManager = machine->getPageMechanism();
        auto processor = machine->getProcessor();

        //Lets read first block off input.
        block_address_t src = slot * EXTERNAL_MEMORY_TASK_SIZE;
        channelManager->getSb()->setValue(CHANNEL_OBJECT_OUTSIDE_MEMORY);
        channelManager->getSt()->setValue(src);
        channelManager->getDb()->setValue(CHANNEL_OBJECT_SUPERVISOR_MEMORY);
        channelManager->getDt()->setValue(SUPERVISOR_UNDEFINED_SEGMENT_OFFSET);
        channelManager->exchange(processor);

        auto header = supervisor->readBlock(SUPERVISOR_UNDEFINED_SEGMENT_OFFSET);
        if (!InstallProcess::isHeaderValid(header)) {
            cout << "Slot " << slot << " doesnt have valid header" << endl;

            this->getResourceManager()->releaseResourceElement(this, RESOURCE_CHANNEL_REGISTER_ID, 0);
            this->getResourceManager()->releaseResourceElement(this, RESOURCE_MEMORY_TASK_ID, 0xFF);
            this->slot = 0xFF;
            this->vmid = 0xFF;
            this->hasChannelManager = false;
            return false;
        }

        auto headerU8 = reinterpret_cast<uint8_t *>(header.data());
        auto headerC = reinterpret_cast<char *>(header.data());
        uint8_t priority = headerU8[3];
        char name[11] = {headerC[2], headerC[1], headerC[0], headerC[7], headerC[6], headerC[5], headerC[4], headerC[11], headerC[10], headerC[9], '\0'};


        auto ptr = pageManager->allocateVirtualMachineMemory();
        channelManager->getSb()->setValue(CHANNEL_OBJECT_OUTSIDE_MEMORY);
        channelManager->getDb()->setValue(CHANNEL_OBJECT_USER_MEMORY);
        for (block_address_t i = 0; i < 10; i++) {
            auto dest = pageManager->getRealBlockFromVirtualBlock(ptr, i);

            channelManager->getDt()->setValue(dest);
            channelManager->getSt()->setValue(++src);
            channelManager->exchange(processor);
        }

        memory_block_t zb = {0};
        supervisor->writeBlock(SUPERVISOR_UNDEFINED_SEGMENT_OFFSET, zb);
        channelManager->getSb()->setValue(CHANNEL_OBJECT_SUPERVISOR_MEMORY);
        channelManager->getSt()->setValue(SUPERVISOR_UNDEFINED_SEGMENT_OFFSET);
        channelManager->getDt()->setValue(pageManager->getRealBlockFromVirtualBlock(ptr, 10));
        channelManager->exchange(processor);

        channelManager->getDt()->setValue(pageManager->getRealBlockFromVirtualBlock(ptr, 11));
        channelManager->exchange(processor);

        this->vmid = this->getManager()->registerProcess(new VMProcess(this->getManager()), priority, name,
                                                         0xFF,
                                                         this->getId());
        auto vmidDesc = this->getManager()->readDescriptorFromMemory(vmid);
        vmidDesc.setPageTable(ptr);
        this->getManager()->writeDescriptorIntoMemory(vmidDesc);

        this->getResourceManager()->releaseResourceElement(this, RESOURCE_CHANNEL_REGISTER_ID, 0);
    }

    return true;
}

void JobGovernorProcess::run() {
    auto res = this->boot();
    if(!res)
        return;

    if(this->jgInterrupt != 0xFF) {
        cout << "JG gavo pranešima apie " << (int) jgInterrupt << " pertraukima!" << endl;

        if(this->jgInterrupt == SI_REGISTER_VALUE_HALT)
            this->getResourceManager()->releaseResourceElement(this, RESOURCE_MEMORY_TASK_ID, 0xFF);
    }

    //Ok so if we are here then VM process is already there.
    this->getResourceManager()->requestResourceElement(this, RESOURCE_JG_INTERRUPT_ID);
    this->jgInterrupt = 0xFF;
    return;
}

void JobGovernorProcess::acceptResourceElement(resource_id_t resourceId, uint8_t data, process_id_t senderId) {
    if (resourceId == RESOURCE_JG_LOAD_TASK_ID) {
        this->slot = data;
        return;
    }

    if (resourceId == RESOURCE_CHANNEL_REGISTER_ID) {
        this->hasChannelManager = true;
        return;
    }

    if(resourceId == RESOURCE_JG_INTERRUPT_ID) {
        this->jgInterrupt = data;
        return;
    }
}


VMProcess::VMProcess(ProcessManager *manager)
        : Process(manager) {

}

void VMProcess::run() {

    //Užkraunam registru reikšmes.
    auto desc = this->getManager()->readDescriptorFromMemory(this->getId());
    auto processor = this->getManager()->getMachine()->getProcessor();

    processor->getR1()->setValue(desc.getR1());
    processor->getR2()->setValue(desc.getR2());
    processor->getRR()->setValue(desc.getRr());
    processor->getIC()->setValue(desc.getIc());
    processor->getC()->setValue(desc.getC());
    processor->getSS()->setValue(desc.getSs());
    processor->getCF()->setValue(desc.getCf());
    processor->getPTR()->setValue(desc.getPageTable());
    processor->getTI()->setValue(10);
    processor->getSI()->setValue(SI_REGISTER_VALUE_OK);

    //
    while (processor->getTI()->getValue() != 0) {
        processor->tick();

        auto si = processor->getSI()->getValue();
        if(si != SI_REGISTER_VALUE_OK)
        {
            this->getResourceManager()->releaseResourceElement(this, RESOURCE_INTERRUPT_ID, si);
            break;
        }
    }

    desc.setR1(processor->getR1()->getValue());
    desc.setR2(processor->getR2()->getValue());
    desc.setRr(processor->getRR()->getValue());
    desc.setIc(processor->getIC()->getValue());
    desc.setC(processor->getC()->getValue());
    desc.setSs(processor->getSS()->getValue());
    desc.setCf(processor->getCF()->getValue());
    desc.setPageTable(processor->getPTR()->getValue());
    this->getManager()->writeDescriptorIntoMemory(desc);
}

void VMProcess::acceptResourceElement(resource_id_t resourceId, uint8_t data, process_id_t senderId) {

}


InterruptService::InterruptService(ProcessManager *manager)
    : Process(manager)
{

}

void InterruptService::run() {

    if(this->senderPID == 0xFF)
    {
        this->getResourceManager()->requestResourceElement(this, RESOURCE_INTERRUPT_ID);
        return;
    }

    auto senderDesc = this->getManager()->readDescriptorFromMemory(senderPID);
    auto jgid = senderDesc.getParentId();
    this->getResourceManager()->releaseResourceElement(this, RESOURCE_JG_INTERRUPT_ID, senderSI, jgid);

    this->senderPID = 0xFF;
    this->senderSI = 0xFF;
    return;
}

void InterruptService::acceptResourceElement(resource_id_t resourceId, uint8_t data, process_id_t senderId) {
    if(resourceId == RESOURCE_INTERRUPT_ID) {
        this->senderPID = senderId;
        this->senderSI = data;
    }
}

TerminalProcess::TerminalProcess(ProcessManager *manager)
        : Process(manager), hasChannelRegister(false) {}

void TerminalProcess::run() {
    auto resourceManager = this->getResourceManager();

    if (!this->hasChannelRegister) {
        resourceManager->requestResourceElement(this, RESOURCE_CHANNEL_REGISTER_ID);
        return;
    }

    auto manager = this->getManager();
    auto machine = manager->getMachine();
    auto memory = machine->getMemory();
    auto supervisor = memory->getSupervisorMemory();
    auto channelManager = machine->getChannelManager();
    auto processor = machine->getProcessor();

    supervisor->writeBlockString(SUPERVISOR_UNDEFINED_SEGMENT_OFFSET, "Įveskite komanda: ");
    supervisor->writeBlockString(SUPERVISOR_UNDEFINED_SEGMENT_OFFSET + 1, "Komanda buvo neatpažinta! P.S. help");

    bool ran = false;
    while (!ran) {
        //lets report for user what to do:
        channelManager->getSb()->setValue(CHANNEL_OBJECT_SUPERVISOR_MEMORY);
        channelManager->getSt()->setValue(SUPERVISOR_UNDEFINED_SEGMENT_OFFSET);
        channelManager->getDb()->setValue(CHANNEL_OBJECT_INPUT_OUTPUT);
        channelManager->getDt()->setValue(1);
        channelManager->exchange(processor);

        //lets read command
        channelManager->getSb()->setValue(CHANNEL_OBJECT_INPUT_OUTPUT);
        channelManager->getSt()->setValue(1);
        channelManager->getDb()->setValue(CHANNEL_OBJECT_SUPERVISOR_MEMORY);
        channelManager->getDt()->setValue(SUPERVISOR_UNDEFINED_SEGMENT_OFFSET + 3);
        channelManager->exchange(processor);

        auto commandBlock = supervisor->readBlock(SUPERVISOR_UNDEFINED_SEGMENT_OFFSET + 3);
        ran = this->runCommand(commandBlock);

        if (!ran) {
            //lets report that commadn was not ran
            channelManager->getSb()->setValue(CHANNEL_OBJECT_SUPERVISOR_MEMORY);
            channelManager->getSt()->setValue(SUPERVISOR_UNDEFINED_SEGMENT_OFFSET + 1);
            channelManager->getDb()->setValue(CHANNEL_OBJECT_INPUT_OUTPUT);
            channelManager->getDt()->setValue(1);
            channelManager->exchange(processor);
        }
    }

    resourceManager->releaseResourceElement(this, RESOURCE_CHANNEL_REGISTER_ID, 0);
    this->hasChannelRegister = false;
}

void TerminalProcess::printHelp() {
    this->printLine("* Pagalbos meniu:");
    this->printLine("* help - Parodo šį meniu!");
    this->printLine("* install - Įrašo programa!");
    this->printLine("* run - Paleidzia programa!");
    this->printLine("* quit - Isjungia kompiuteri!");
}

void TerminalProcess::printLine(const std::string &line) {
    auto manager = this->getManager();
    auto machine = manager->getMachine();
    auto memory = machine->getMemory();
    auto supervisor = memory->getSupervisorMemory();
    auto channelManager = machine->getChannelManager();
    auto processor = machine->getProcessor();

    supervisor->writeBlockString(SUPERVISOR_UNDEFINED_SEGMENT_OFFSET + 2, line);
    channelManager->getSb()->setValue(CHANNEL_OBJECT_SUPERVISOR_MEMORY);
    channelManager->getSt()->setValue(SUPERVISOR_UNDEFINED_SEGMENT_OFFSET + 2);
    channelManager->getDb()->setValue(CHANNEL_OBJECT_INPUT_OUTPUT);
    channelManager->getDt()->setValue(1);
    channelManager->exchange(processor);
}

bool TerminalProcess::runCommand(memory_block_t commandBlock) {
    char cmd[65] = {'\0'};
    memcpy(cmd, commandBlock.data(), 64 * sizeof(char));

    string _cmd(cmd);
    stringstream sstream(_cmd);

    std::string command;
    sstream >> command;

    if (command == "help") {
        this->printHelp();
        return true;
    }

    if (command == "install") {
        if (sstream.eof()) {
            this->printLine("Naudojimas: install <slot>.");
            return false;
        }

        int slot;
        sstream >> slot;
        if (slot < 0 || slot >= 16) {
            this->printLine("Slot turetu buti tarp 0 ir 15!");
            return false;
        }

        this->getResourceManager()->releaseResourceElement(this, RESOURCE_INSTALL_ID, slot);
        return true;
    }

    if (command == "debug") {
        this->getManager()->getMachine()->debugMenu();
        return true;
    }

    if (command == "run") {
        if (sstream.eof()) {
            this->printLine("Naudojimas: run <slot>.");
            return false;
        }

        int slot;
        sstream >> slot;
        if (slot < 0 || slot >= 16) {
            this->printLine("Slot turetu buti tarp 0 ir 15!");
            return false;
        }

        this->getResourceManager()->releaseResourceElement(this, RESOURCE_MEMORY_TASK_ID, slot);
        return true;
    }

    if (command == "quit") {
        this->getResourceManager()->releaseResourceElement(this, RESOURCE_MACHINE_STOP_ID, 0);
        return true;
    }

    return false;
}

void TerminalProcess::acceptResourceElement(resource_id_t resourceId, uint8_t data, process_id_t senderId) {
    if (resourceId == RESOURCE_CHANNEL_REGISTER_ID)
        this->hasChannelRegister = true;
}