//
// Created by kudze on 2022-03-24.
//

#ifndef LAB04_PROCESSOR_H
#define LAB04_PROCESSOR_H

#include "registers.h"
#include "memory.h"
#include "instructions.h"

class RealMachine;

#define SI_REGISTER_VALUE_OK 0
#define SI_REGISTER_VALUE_HALT 1
#define SI_REGISTER_VALUE_PB 2
#define SI_REGISTER_VALUE_GB 4

typedef uint8_t register_index_t;
#define REGISTER_INDEX_R1 0
#define REGISTER_INDEX_R2 1
#define REGISTER_INDEX_RR 2
#define REGISTER_INDEX_IC 3
#define REGISTER_INDEX_C  4
#define REGISTER_INDEX_SS 5
#define REGISTER_INDEX_CF 6
#define REGISTER_INDEX_TI 7
#define REGISTER_INDEX_SI 8
#define REGISTER_INDEX_PTR 9
#define REGISTER_INDEX_MIN REGISTER_INDEX_R1
#define REGISTER_INDEX_MAX REGISTER_INDEX_PTR

class Processor {

    //Duomenų registrai, naudojami žodžių užkrovimui/išsaugojimui, operacijose
    Int32Register* r1;
    Int32Register* r2;
    Int32Register* rr; //< -- I šita lipdo EXHANCGE rezultata.

    //Einamosios komandos žodžio indekso registras
    UInt16Register* ic;

    //Skaitliuko registras naudojamas cikluose
    UInt32Register* c;

    //Steko segmento viršūnės indeksas
    UInt8Register* ss;

    //Palyginimo operacijos reikšmės registras
    UInt8Register* cf;

    //Taimerio registras
    UInt8Register* ti;

    //Supervizorinių pertraukimų registras
    UInt8Register* si;

    //Ptr registras
    PtrRegister* ptr;

    RealMachine* machine;

public:

    /**
     * Instructions.
     */
    void inst_add(register_index_t firstRegisterIndex, register_index_t secondRegisterIndex);
    void inst_sub(register_index_t firstRegisterIndex, register_index_t secondRegisterIndex);
    void inst_mul(register_index_t firstRegisterIndex, register_index_t secondRegisterIndex);
    void inst_div(register_index_t firstRegisterIndex, register_index_t secondRegisterIndex);
    void inst_neg(register_index_t targetRegisterIndex);
    void inst_inc(register_index_t targetRegisterIndex);
    void inst_dec(register_index_t targetRegisterIndex);

    void inst_and(uint8_t firstRegisterIndex, uint8_t secondRegisterIndex);
    void inst_or(uint8_t firstRegisterIndex, uint8_t secondRegisterIndex);
    void inst_not(uint8_t targetRegisterIndex);
    void inst_lshift(register_index_t targetRegisterIndex, uint8_t data);
    void inst_rshift(register_index_t targetRegisterIndex, uint8_t data);

    void inst_cmp(uint8_t firstRegisterIndex, uint8_t secondRegisterIndex);

    void inst_jm(uint8_t firstByte, uint8_t secondByte);
    void inst_jl(uint8_t firstByte, uint8_t secondByte);
    void inst_je(uint8_t firstByte, uint8_t secondByte);
    void inst_jg(uint8_t firstByte, uint8_t secondByte);
    void inst_call(uint8_t firstByte, uint8_t secondByte);
    void inst_ret();
    void inst_loop(uint8_t firstByte, uint8_t secondByte);
    void inst_halt();

    void inst_pb(register_index_t blockAddress);
    void inst_gb(register_index_t blockAddress);
    void inst_exchange();

    void inst_ssr(register_index_t registerIndex);
    void inst_lsr(register_index_t registerIndex);

    void inst_lwrr(register_index_t sourceRegisterIndex, register_index_t destRegisterIndex);
    void inst_lwrc(register_index_t destRegisterIndex, memory_word_t data);
    void inst_lwrm(register_index_t registerIndex, block_address_t blockAddress, word_address_t wordAddress);
    void inst_lwmr(block_address_t blockAddress, word_address_t wordAddress, register_index_t registerIndex);

protected:
    /**
     * Stores/Retrieves from memory to register or otherwise.
     */
    Int32Register* getMemoryRegisterFromIndex(register_index_t idx) const;
    memory_word_t getWordFromRegister(register_index_t idx) const;
    void writeWordIntoRegister(register_index_t idx, memory_word_t word) const;

    /**
     * Some helpers for tick.
     */
    memory_word_t readNextInstructionFromMemory() const;
    uint8_t runInstruction(memory_word_t instruction);

    void handleInterrupts();

public:

    explicit Processor(RealMachine* machine);
    ~Processor();

    //Handles one ic increase per tick.
    uint8_t tick();

    void print();

    Int32Register* getR1() const;
    Int32Register* getR2() const;
    Int32Register* getRR() const;
    UInt16Register* getIC() const;
    UInt32Register* getC() const;
    UInt8Register* getSS() const;
    UInt8Register* getCF() const;
    UInt8Register* getTI() const;
    UInt8Register* getSI() const;
    PtrRegister* getPTR() const;
    RealMachine *getMachine() const;
};


#endif //LAB04_PROCESSOR_H
