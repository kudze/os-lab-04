//
// Created by kudze on 2022-05-29.
//

#ifndef LAB04_RESOURCE_H
#define LAB04_RESOURCE_H

#define MAX_RESOURCES_COUNT SUPERVISOR_RESOURCE_DESCRIPTOR_SIZE

#include <cstdint>

class ResourceManager;
typedef uint8_t resource_id_t;

#include "real_machine.h"

class ResourceElementDescriptor {
    uint8_t data;
    process_id_t receiverId; //0xFF means that any process can get this element.
    process_id_t senderId;

public:
    ResourceElementDescriptor(uint8_t data = 0xFF, process_id_t receiverId = 0xFF, process_id_t senderId = 0xFF);
    ~ResourceElementDescriptor() = default;

    uint8_t getData() const;
    process_id_t getReceiverId() const;
    process_id_t getSenderId() const;

    void setData(uint8_t data);
    void setReceiverId(process_id_t receiverId);
    void setSenderId(process_id_t senderId);
};

class ResourceDescriptor {
    resource_id_t id;
    process_id_t ownerId;
    process_id_t waitingIds[16] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};
    uint8_t elementCount = 0;
    ResourceElementDescriptor elements[15] = {};

public:
    ResourceDescriptor(resource_id_t id, process_id_t ownerId);
    explicit ResourceDescriptor(memory_block_t block);

    memory_block_t toBlock() const;
    resource_id_t getId() const;
    process_id_t getOwnerId() const;
    const process_id_t *getWaitingIds() const;
    uint8_t getElementCount() const;
    const ResourceElementDescriptor *getElements() const;

    void removeWaitingByValue(process_id_t pid);
    void removeWaiting(uint8_t id);
    void removeElement(uint8_t id);

    void addProcessToWaitingList(process_id_t processId, uint8_t priority, ProcessManager* processManager);
    void addElement(ResourceElementDescriptor const& elementDescriptor);

    bool schedule(ProcessManager* processManager);
};

class ResourceManager {
    RealMachine* machine;

    resource_id_t findFirstUnusedResourceId() const;

public:
    explicit ResourceManager(RealMachine* machine);
    ~ResourceManager() = default;

    resource_id_t registerResource(process_id_t ownerId, resource_id_t resourceId = 0xFF);
    void unregisterResource(resource_id_t resourceId);
    void requestResourceElement(Process* process, resource_id_t resourceId);
    void releaseResourceElement(Process* process, resource_id_t resourceId, uint8_t data, process_id_t targetProcess = 0xFF);

    void writeDescriptorIntoMemory(ResourceDescriptor const& descriptor) const;
    ResourceDescriptor readDescriptorFromMemory(resource_id_t resourceId) const;
    void deleteDescriptorFromMemory(resource_id_t resourceId) const;
    bool isDescriptorSlotUsed(resource_id_t resourceId) const;

    void callScheduler();

    void print() const;

    RealMachine *getMachine() const;
};


#endif //LAB04_RESOURCE_H
