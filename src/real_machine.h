//
// Created by kudze on 2022-03-24.
//

#ifndef LAB04_REAL_MACHINE_H
#define LAB04_REAL_MACHINE_H

#include "processor.h"
#include "memory.h"
#include "channel_manager.h"
#include "page_mechanism.h"
#include "external_memory.h"
#include "process.h"
#include "resource.h"

class RealMachine {
    bool shouldContinue;
    bool debug;

    Processor* processor;
    Memory* memory;
    ChannelManager* channelManager;
    PageMechanism* pageMechanism;
    ExternalMemory* externalMemory;

    ProcessManager* processManager;
    ResourceManager* resourceManager;

    void boot();
    void shutdown();
    void tick();

public:
    RealMachine(bool debug = false);
    ~RealMachine();

    void run();
    void stopOnNextTick();

    void debugMenu();

    bool getShouldContinue() const;
    bool isDebug() const;
    Processor* getProcessor() const;
    Memory* getMemory() const;
    ChannelManager* getChannelManager() const;
    PageMechanism *getPageMechanism() const;
    ExternalMemory *getExternalMemory() const;

    ProcessManager* getProcessManager() const;

    ResourceManager *getResourceManager() const;
};

#endif //LAB04_REAL_MACHINE_H
