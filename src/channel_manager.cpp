//
// Created by kudze on 2022-03-25.
//

#include "channel_manager.h"
#include "real_machine.h"

#include <iostream>
#include <iomanip>
#include <cstring>

using namespace std;

ChannelManager::ChannelManager() {
    this->sb = new UInt8Register(0);
    this->db = new UInt8Register(0);
    this->st = new UInt8Register(0);
    this->dt = new UInt8Register(0);
}

ChannelManager::~ChannelManager() {
    delete this->sb;
    delete this->db;
    delete this->st;
    delete this->dt;
}

void ChannelManager::exchange(Processor* processor) {
    memory_block_t data = this->readSource(processor);
    this->writeDestination(processor, data);
}

void ChannelManager::writeDestination(Processor *processor, const memory_block_t &data) {
    auto destinationBuffer = this->getDb()->getValue();
    auto destinationAddress = this->getDt()->getValue();

    //Output print.
    switch (destinationBuffer) {
        case CHANNEL_OBJECT_USER_MEMORY: {
            auto memory = processor->getMachine()->getMemory();
            memory->getUserMemory()->writeBlock(destinationAddress, data);
            break;
        }
        case CHANNEL_OBJECT_SUPERVISOR_MEMORY: {
            auto memory = processor->getMachine()->getMemory();
            memory->getSupervisorMemory()->writeBlock(destinationAddress, data);
            break;
        }
        case CHANNEL_OBJECT_OUTSIDE_MEMORY: {
            auto memory = processor->getMachine()->getExternalMemory();
            memory->writeBlock(destinationAddress, data);
            break;
        }
        case CHANNEL_OBJECT_INPUT_OUTPUT: {
            char str[65];

            uint8_t idx = 0;
            for (memory_word_t word: data) {
                auto word_ptr = reinterpret_cast<uint8_t *>(&word);

                for (int i = 0; i < 4; i++)
                    str[idx++] = word_ptr[i];
            }
            str[idx] = '\0';
            cout << "[INFO] Machine output [window = " << (int) destinationAddress << "]: " << str << endl;

            break;
        }
        case CHANNEL_OBJECT_INPUT_OUTPUT_HEX: {
            cout << "[INFO] Machine output [window = " << (int) destinationAddress << "]: ";

            for (memory_word_t word: data)
                std::cout << std::hex << std::setfill('0') << std::setw(8) << word << " ";

            std::cout << std::endl;
            break;
        }
        default:
            throw std::runtime_error("Unknown destination bus in exchange instruction!");
    }

}

memory_block_t ChannelManager::readSource(Processor *processor) {
    auto sourceBuffer = this->getSb()->getValue();
    auto sourceBlockAddress = this->getSt()->getValue();

    switch (sourceBuffer) {
        case CHANNEL_OBJECT_USER_MEMORY: {
            auto memory = processor->getMachine()->getMemory();
            return memory->getUserMemory()->readBlock(sourceBlockAddress);
        }
        case CHANNEL_OBJECT_SUPERVISOR_MEMORY: {
            auto memory = processor->getMachine()->getMemory();
            return memory->getSupervisorMemory()->readBlock(sourceBlockAddress);
        }
        case CHANNEL_OBJECT_OUTSIDE_MEMORY: {
            auto memory = processor->getMachine()->getExternalMemory();
            return memory->readBlock(sourceBlockAddress);
        }
        case CHANNEL_OBJECT_INPUT_OUTPUT: {
            char message[65] = {0};

            cout << "[INFO] Machine is asking for input [window = " << (int) sourceBlockAddress << "]: ";
            do {
                cin.getline(message, 65, '\n');
            } while (strlen(message) == 0);

            memory_block_t data;
            auto word_ptr = reinterpret_cast<memory_word_t *>(&message);
            for (int i = 0; i < MEMORY_BLOCK_SIZE; i++)
                data[i] = word_ptr[i];

            return data;
        }
        case CHANNEL_OBJECT_INPUT_OUTPUT_HEX: {
            cout << "[INFO] Machine is asking for hex block input [window = " << (int) sourceBlockAddress << "]: "
                 << endl;

            memory_block_t data;
            for (block_address_t i = 0; i < MEMORY_BLOCK_SIZE; i++)
                cin >> hex >> data[i];

            return data;
        }
        default:
            throw std::runtime_error("Unknown source bus in exchange instruction!");
    }
}

UInt8Register *ChannelManager::getSb() const {
    return sb;
}

UInt8Register *ChannelManager::getDb() const {
    return db;
}

UInt8Register *ChannelManager::getSt() const {
    return st;
}

UInt8Register *ChannelManager::getDt() const {
    return dt;
}