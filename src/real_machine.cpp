#include "real_machine.h"
#include "os_process.h"

#include <iostream>

using namespace std;

RealMachine::RealMachine(bool debug) : shouldContinue(true), debug(debug) {
    this->processor = new Processor(this);
    this->memory = new Memory();
    this->externalMemory = new ExternalMemory();
    this->channelManager = new ChannelManager();
    this->pageMechanism = new PageMechanism(this->getMemory());

    this->processManager = new ProcessManager(this);
    this->resourceManager = new ResourceManager(this);
}

RealMachine::~RealMachine() {
    delete this->resourceManager;
    delete this->processManager;

    delete this->pageMechanism;
    delete this->channelManager;
    delete this->externalMemory;
    delete this->memory;
    delete this->processor;
}

void RealMachine::run() {
    this->boot();

    while (this->getShouldContinue())
        this->tick();

    this->shutdown();

    if (this->isDebug())
        this->debugMenu();
}

void RealMachine::boot() {
    cout << "RealMachine::boot kuria BootProcess procesą..." << endl;

    processManager->registerProcess(
            new BootProcess(processManager),
            255,
            "Boot",
            PROCESS_BOOT_ID
    );
}

void RealMachine::shutdown() {
    cout << "RealMachine::shutdown naikina BootProcess procesą..." << endl;

    processManager->unregisterProcess(PROCESS_BOOT_ID);
}

void RealMachine::debugMenu() {
    while (true) {
        cout << endl
             << "What to do now?" << endl
             << "Possibilities: " << endl
             << "* continue - runs next process scheduler" << endl
             << "* processor - dumps processor data" << endl
             << "* supervisor - dumps supervisor data" << endl
             << "* user - dumps user data" << endl
             << "* external - dumps external data" << endl
             << "* process_descriptors (pd) - shows process descriptors" << endl
             << "* resource_descriptors (rd) - shows resource descriptors" << endl << endl

             << "Input: ";

        string input;
        cin >> input;

        if (input == "continue" || input == "c")
            break;

        if (input == "processor" || input == "p") {
            this->getProcessor()->print();
            continue;
        }

        if (input == "supervisor" || input == "s") {
            this->getMemory()->getSupervisorMemory()->print();
            continue;
        }

        if (input == "user" || input == "u") {
            this->getMemory()->getUserMemory()->print();
            continue;
        }

        if (input == "external" || input == "e") {
            this->getExternalMemory()->print();
            continue;
        }

        if (input == "process_descriptors" || input == "pd") {
            this->getProcessManager()->print();
            continue;
        }

        if (input == "resource_descriptors" || input == "rd") {
            this->getResourceManager()->print();
            continue;
        }

        cout << "Unrecognized input!" << endl << endl;
    }
}

void RealMachine::tick() {
    //auto instructionType = processor->tick();
    this->processManager->runPlanner();

    if (this->isDebug())
        this->debugMenu();
}

void RealMachine::stopOnNextTick() {
    this->shouldContinue = false;
}

bool RealMachine::getShouldContinue() const {
    return this->shouldContinue;
}

bool RealMachine::isDebug() const {
    return this->debug;
}

Processor *RealMachine::getProcessor() const {
    return this->processor;
}

Memory *RealMachine::getMemory() const {
    return this->memory;
}

ChannelManager *RealMachine::getChannelManager() const {
    return this->channelManager;
}

PageMechanism *RealMachine::getPageMechanism() const {
    return pageMechanism;
}

ExternalMemory *RealMachine::getExternalMemory() const {
    return externalMemory;
}

ProcessManager *RealMachine::getProcessManager() const {
    return this->processManager;
}

ResourceManager *RealMachine::getResourceManager() const {
    return resourceManager;
}
