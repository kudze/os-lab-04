//
// Created by kudze on 2022-05-29.
//

#include "resource.h"

#include <iostream>
#include <iomanip>

using namespace std;

ResourceElementDescriptor::ResourceElementDescriptor(uint8_t data, process_id_t receiverId, process_id_t senderId)
        : data(data), receiverId(receiverId), senderId(senderId) {}

uint8_t ResourceElementDescriptor::getData() const {
    return data;
}

process_id_t ResourceElementDescriptor::getReceiverId() const {
    return receiverId;
}

process_id_t ResourceElementDescriptor::getSenderId() const {
    return senderId;
}

void ResourceElementDescriptor::setData(uint8_t data) {
    ResourceElementDescriptor::data = data;
}

void ResourceElementDescriptor::setReceiverId(process_id_t receiverId) {
    ResourceElementDescriptor::receiverId = receiverId;
}

void ResourceElementDescriptor::setSenderId(process_id_t senderId) {
    ResourceElementDescriptor::senderId = senderId;
}

ResourceDescriptor::ResourceDescriptor(resource_id_t id, process_id_t ownerId)
        : id(id), ownerId(ownerId) {
}

ResourceDescriptor::ResourceDescriptor(memory_block_t block) {
    auto resultU8 = reinterpret_cast<uint8_t *>(block.data());
    this->id = resultU8[0];
    this->ownerId = resultU8[1];

    //2-17
    for (uint8_t i = 0; i < 16; i++)
        this->waitingIds[i] = resultU8[i + 2];

    this->elementCount = resultU8[18];

    for (uint8_t i = 0; i < 15; i++) {
        this->elements[i].setData(resultU8[(i * 3) + 19]);
        this->elements[i].setReceiverId(resultU8[(i * 3) + 20]);
        this->elements[i].setSenderId(resultU8[(i * 3) + 21]);
    }
}

memory_block_t ResourceDescriptor::toBlock() const {
    memory_block_t result = {0};
    auto resultU8 = reinterpret_cast<uint8_t *>(result.data());

    resultU8[0] = this->id;
    resultU8[1] = this->ownerId;

    for (uint8_t i = 0; i < 16; i++)
        resultU8[i + 2] = this->waitingIds[i];

    resultU8[18] = this->elementCount;

    for (uint8_t i = 0; i < 15; i++) {
        resultU8[(i * 3) + 19] = this->elements[i].getData();
        resultU8[(i * 3) + 20] = this->elements[i].getReceiverId();
        resultU8[(i * 3) + 21] = this->elements[i].getSenderId();
    }

    return result;
}

void
ResourceDescriptor::addProcessToWaitingList(process_id_t processId, uint8_t priority, ProcessManager *processManager) {

    for (uint8_t i = 0; i < 16; i++) {
        if (waitingIds[i] == 0xFF) {
            waitingIds[i] = processId;
            return;
        }

        auto descriptor = processManager->readDescriptorFromMemory(waitingIds[i]);
        if (descriptor.getPriority() < priority) {
            for (uint8_t j = 16 - 1; j-- > i;)
                waitingIds[j + 1] = waitingIds[j];

            waitingIds[i] = processId;
            return;
        }
    }

    throw std::runtime_error("Couldn't add process to resource's waitingIds since array is full!");
}

void ResourceDescriptor::addElement(const ResourceElementDescriptor &elementDescriptor) {
    if (this->elementCount == 15)
        throw std::runtime_error("Failed to add element, to resource, because element array is already full!");

    this->elements[this->elementCount++] = elementDescriptor;
}

bool ResourceDescriptor::schedule(ProcessManager *processManager) {
    if (this->elementCount == 0)
        return false;

    bool changed = false;

    for (uint8_t i = 0; i < MAX_PROCESS_COUNT; i++) {
        //they are already sorted by priority.
        auto pid = this->waitingIds[i];
        if (pid == 0xFF)
            break; //we can break cuz if we found 0xFF means all rest will be 0xFF (unused).

        auto pdesc = processManager->readDescriptorFromMemory(pid);
        if (pdesc.getState() == BLOCKED_STOPPED) //READY_STOPPED should be fine, since it won't be here then.
            continue;

        bool repeat = false;

        for (uint8_t j = 0; j < this->elementCount; j++) {
            auto element = this->elements[j];
            auto receiverId = element.getReceiverId();
            if (receiverId != 0xFF && receiverId != pid)
                continue;

            auto process = processManager->getProcessById(pid);
            process->acceptResourceElement(this->id, element.getData(), element.getSenderId());

            pdesc.setState(READY);
            pdesc.setWaitingResourceId(0xFF);
            processManager->writeDescriptorIntoMemory(pdesc);

            this->removeWaiting(i);
            this->removeElement(j);
            repeat = true;
            changed = true;
            break;
        }

        if (repeat) i--;
    }

    return changed;
}

void ResourceDescriptor::removeElement(uint8_t id) {
    this->elementCount--;

    for (uint8_t i = id; i < this->elementCount; i++)
        this->elements[i] = this->elements[i + 1];

    this->elements[this->elementCount] = ResourceElementDescriptor();
}

void ResourceDescriptor::removeWaitingByValue(process_id_t pid) {
    for (uint8_t i = 0; i < MAX_PROCESS_COUNT; i++)
        if (this->waitingIds[i] == pid) {
            this->removeWaiting(i);
            return;
        }
}

void ResourceDescriptor::removeWaiting(uint8_t _id) {
    uint8_t maxProcessCountMin1 = MAX_PROCESS_COUNT - 1;
    for (uint8_t i = _id; i < MAX_PROCESS_COUNT - 1; i++)
        this->waitingIds[i] = this->waitingIds[i + 1];

    this->waitingIds[maxProcessCountMin1] = 0xFF;
}

resource_id_t ResourceDescriptor::getId() const {
    return id;
}

process_id_t ResourceDescriptor::getOwnerId() const {
    return ownerId;
}

const process_id_t *ResourceDescriptor::getWaitingIds() const {
    return waitingIds;
}

uint8_t ResourceDescriptor::getElementCount() const {
    return elementCount;
}

const ResourceElementDescriptor *ResourceDescriptor::getElements() const {
    return elements;
}

ResourceManager::ResourceManager(RealMachine *machine)
        : machine(machine) {}

RealMachine *ResourceManager::getMachine() const {
    return machine;
}

resource_id_t ResourceManager::findFirstUnusedResourceId() const {
    for (process_id_t i = 0; i < MAX_RESOURCES_COUNT; i++)
        if (!this->isDescriptorSlotUsed(i))
            return i;

    throw runtime_error("There are no free resource descriptors to allocate!");
}

resource_id_t ResourceManager::registerResource(process_id_t ownerId, resource_id_t resourceId) {
    if (resourceId == 0xFF)
        resourceId = this->findFirstUnusedResourceId();

    cout << "Procesas su id " << (int) ownerId << " sukūrė resursą, kurio id: " << (int) resourceId << endl;

    auto descriptor = ResourceDescriptor(resourceId, ownerId);
    this->writeDescriptorIntoMemory(descriptor);

    return resourceId;
}

void ResourceManager::unregisterResource(resource_id_t resourceId) {
    auto processManager = this->getMachine()->getProcessManager();
    auto descriptor = this->readDescriptorFromMemory(resourceId);

    for (uint8_t i = 0; i < MAX_PROCESS_COUNT; i++) {
        auto pid = descriptor.getWaitingIds()[i];
        if (pid == 0xFF)
            break;

        processManager->removeDescriptorFromMemory(pid);
    }

    this->deleteDescriptorFromMemory(resourceId);
}

void ResourceManager::requestResourceElement(Process *process, resource_id_t resourceId) {
    auto processManager = this->getMachine()->getProcessManager();
    auto procDesc = processManager->readDescriptorFromMemory(process->getId());
    procDesc.setState(BLOCKED);
    procDesc.setWaitingResourceId(resourceId);
    processManager->writeDescriptorIntoMemory(procDesc);

    cout << "Process " << procDesc.getTitle() << " (" << (int) procDesc.getId() << ") has requested resource with id: "
         << (int) resourceId << endl;

    auto resourceDesc = this->readDescriptorFromMemory(resourceId);
    resourceDesc.addProcessToWaitingList(process->getId(), procDesc.getPriority(), process->getManager());
    this->writeDescriptorIntoMemory(resourceDesc);

    this->callScheduler();
}

void ResourceManager::releaseResourceElement(Process *process, resource_id_t resourceId, uint8_t data,
                                             process_id_t targetProcess) {
    auto processManager = this->getMachine()->getProcessManager();
    auto procDesc = processManager->readDescriptorFromMemory(process->getId());

    cout << "Process " << procDesc.getTitle() << " (" << (int) procDesc.getId() << ") has release resource with id: "
         << (int) resourceId << ", data = " << (int) data;

    if (targetProcess != 0xFF)
        cout << ", targetProcess = " << (int) targetProcess;
    cout << endl;

    auto resourceDesc = this->readDescriptorFromMemory(resourceId);
    resourceDesc.addElement(ResourceElementDescriptor(data, targetProcess, process->getId()));
    this->writeDescriptorIntoMemory(resourceDesc);

    this->callScheduler();
}

void ResourceManager::writeDescriptorIntoMemory(const ResourceDescriptor &descriptor) const {
    auto block = descriptor.toBlock();
    auto memory = this->getMachine()->getMemory();

    memory->getSupervisorMemory()->writeBlock(SUPERVISOR_RESOURCE_DESCRIPTOR_OFFSET + descriptor.getId(), block);
}

ResourceDescriptor ResourceManager::readDescriptorFromMemory(resource_id_t resourceId) const {
    auto memory = this->getMachine()->getMemory();
    auto block = memory->getSupervisorMemory()->readBlock(SUPERVISOR_RESOURCE_DESCRIPTOR_OFFSET + resourceId);

    if (Memory::isMemoryBlockZeroed(block))
        throw std::out_of_range("ResourceManager::readDescriptorFromMemory was called with unused resourceId");

    return ResourceDescriptor(block);
}

void ResourceManager::deleteDescriptorFromMemory(resource_id_t resourceId) const {
    auto memory = this->getMachine()->getMemory();
    memory_block_t block = {0};

    memory->getSupervisorMemory()->writeBlock(SUPERVISOR_RESOURCE_DESCRIPTOR_OFFSET + resourceId, block);
}

bool ResourceManager::isDescriptorSlotUsed(resource_id_t resourceId) const {
    auto memory = this->getMachine()->getMemory();
    auto block = memory->getSupervisorMemory()->readBlock(SUPERVISOR_RESOURCE_DESCRIPTOR_OFFSET + resourceId);

    return !Memory::isMemoryBlockZeroed(block);
}

void ResourceManager::callScheduler() {
    cout << "Resource scheduler has been called!" << endl;

    auto processManager = this->getMachine()->getProcessManager();

    for (uint8_t i = 0; i < MAX_RESOURCES_COUNT; i++) {
        try {
            ResourceDescriptor resourceDescriptor = this->readDescriptorFromMemory(i);

            auto changed = resourceDescriptor.schedule(processManager);

            if (changed)
                this->writeDescriptorIntoMemory(resourceDescriptor);
        } catch (out_of_range const &error) {
            continue;
        }
    }
}

void ResourceManager::print() const {
    cout << "Printing resources..." << endl;

    for (resource_id_t i = 0; i < MAX_RESOURCES_COUNT; i++) {
        cout << '[' << setw(2) << (int) i << "]: ";

        try {
            auto descriptor = this->readDescriptorFromMemory(i);

            cout << "ownerid = " << (int) descriptor.getOwnerId() << endl;
            cout << "*** Waiting: ";
            for (uint8_t j = 0; j < MAX_RESOURCES_COUNT; j++) {
                auto waitingId = descriptor.getWaitingIds()[j];

                if (waitingId == 0xFF)
                    break;

                if (j != 0)
                    cout << ", ";

                cout << (int) waitingId;
            }
            cout << endl;

            cout << "*** Elements [" << (int) descriptor.getElementCount() << "]: " << endl;
            for (uint8_t j = 0; j < descriptor.getElementCount(); j++) {
                auto element = descriptor.getElements()[j];
                cout << "*** " << '[' << setw(2) << (int) j << "]: ";

                cout << "data = " << (int) element.getData() << ", ";
                cout << "sender process = " << (int) element.getSenderId();

                auto receiverId = element.getReceiverId();
                if (receiverId != 0xFF)
                    cout << ", receiver process = " << (int) receiverId;

                cout << endl;
            }

            cout << endl;
        } catch (std::out_of_range const &e) {
            cout << "Nenaudojamas!" << endl;
        }
    }
}
