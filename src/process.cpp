//
// Created by kudze on 2022-05-29.
//

#include "process.h"

#include <cstring>

#include <iostream>
#include <iomanip>

using namespace std;

std::string processStatesString[] = {
        "Running",
        "Ready",
        "Blocked",
        "Ready Stopped",
        "Blocked Stopped"
};

std::string convertProcessStateToString(ProcessState state) {
    return processStatesString[state];
}

ProcessDescriptor::ProcessDescriptor(uint8_t priority, const char *title, process_id_t id, process_id_t parentId)
        : id(id), parentId(parentId), state(READY), priority(priority) {
    if (strlen(title) > 10)
        throw std::invalid_argument("ProcessDescriptor title should be shorter or equal to 10 characters!");

    strcpy(this->title, title);
}

ProcessDescriptor::ProcessDescriptor(memory_block_t block) {
    auto resultU8 = reinterpret_cast<uint8_t *>(block.data());

    this->id = resultU8[0];
    this->parentId = resultU8[1];

    //2-17
    for (uint8_t i = 0; i < 16; i++)
        this->childIds[i] = resultU8[i + 2];

    this->state = static_cast<ProcessState>(resultU8[18]);
    this->priority = resultU8[19];

    strcpy(this->title, reinterpret_cast<char *>(resultU8 + 20));

    auto _r1 = reinterpret_cast<uint8_t *>(&this->r1);
    auto _r2 = reinterpret_cast<uint8_t *>(&this->r2);
    auto _rr = reinterpret_cast<uint8_t *>(&this->rr);
    auto _ic = reinterpret_cast<uint8_t *>(&this->ic);
    auto _c = reinterpret_cast<uint8_t *>(&this->c);

    _r1[0] = resultU8[32];
    _r1[1] = resultU8[33];
    _r1[2] = resultU8[34];
    _r1[3] = resultU8[35];

    _r2[0] = resultU8[36];
    _r2[1] = resultU8[37];
    _r2[2] = resultU8[38];
    _r2[3] = resultU8[39];

    _rr[0] = resultU8[40];
    _rr[1] = resultU8[41];
    _rr[2] = resultU8[42];
    _rr[3] = resultU8[43];

    _ic[0] = resultU8[44];
    _ic[1] = resultU8[45];

    _c[0] = resultU8[46];
    _c[1] = resultU8[47];
    _c[2] = resultU8[48];
    _c[3] = resultU8[49];

    this->ss = resultU8[50];
    this->cf = resultU8[51];

    this->pageTable.expanded.unused = resultU8[52];
    this->pageTable.expanded.pageTableSize = resultU8[53];
    this->pageTable.expanded.realBlockAddressSize = resultU8[54];
    this->pageTable.expanded.realWordAddressSize = resultU8[55];

    this->waitingResourceId = resultU8[56];
}

memory_block_t ProcessDescriptor::toBlock() const {
    memory_block_t result = {0};

    auto resultU8 = reinterpret_cast<uint8_t *>(result.data());
    resultU8[0] = this->getId();
    resultU8[1] = this->getParentId();

    //2-17
    for (uint8_t i = 0; i < 16; i++)
        resultU8[i + 2] = this->getChildIds()[i];

    resultU8[18] = static_cast<uint8_t>(this->getState());
    resultU8[19] = this->getPriority();

    //20-31
    strcpy(reinterpret_cast<char *>(resultU8 + 20), this->getTitle());

    auto _r1 = reinterpret_cast<uint8_t const *>(&this->r1);
    auto _r2 = reinterpret_cast<uint8_t const *>(&this->r2);
    auto _rr = reinterpret_cast<uint8_t const *>(&this->rr);
    auto _ic = reinterpret_cast<uint8_t const *>(&this->ic);
    auto _c = reinterpret_cast<uint8_t const *>(&this->c);

    resultU8[32] = _r1[0];
    resultU8[33] = _r1[1];
    resultU8[34] = _r1[2];
    resultU8[35] = _r1[3];

    resultU8[36] = _r2[0];
    resultU8[37] = _r2[1];
    resultU8[38] = _r2[2];
    resultU8[39] = _r2[3];

    resultU8[40] = _rr[0];
    resultU8[41] = _rr[1];
    resultU8[42] = _rr[2];
    resultU8[43] = _rr[3];

    resultU8[44] = _ic[0];
    resultU8[45] = _ic[1];

    resultU8[46] = _c[0];
    resultU8[47] = _c[1];
    resultU8[48] = _c[2];
    resultU8[49] = _c[3];

    resultU8[50] = ss;
    resultU8[51] = cf;

    resultU8[52] = pageTable.expanded.unused;
    resultU8[53] = pageTable.expanded.pageTableSize;
    resultU8[54] = pageTable.expanded.realBlockAddressSize;
    resultU8[55] = pageTable.expanded.realWordAddressSize;

    resultU8[56] = waitingResourceId;

    return result;
}

void ProcessDescriptor::addChildProcess(process_id_t childId) {
    for (uint8_t i = 0; i < MAX_PROCESS_COUNT; i++) {
        if (this->childIds[i] == 0xFF) {
            this->childIds[i] = childId;
            return;
        }
    }

    throw std::runtime_error("Unable to assing child process to parent descriptor because array is full!");
}

void ProcessDescriptor::removeChildProcess(uint8_t index) {
    for (uint8_t i = index; i < MAX_PROCESS_COUNT - 1; i++)
        this->childIds[i] = this->childIds[i + 1];

    this->childIds[MAX_PROCESS_COUNT - 1] = 0xFF;
}

void ProcessDescriptor::removeChildProcessByValue(process_id_t childId) {
    for (uint8_t i = 0; i < MAX_PROCESS_COUNT; i++)
        if (this->childIds[i] == childId) {
            this->removeChildProcess(i);
            return;
        }
}

process_id_t ProcessDescriptor::getId() const {
    return id;
}

process_id_t ProcessDescriptor::getParentId() const {
    return parentId;
}

const process_id_t *ProcessDescriptor::getChildIds() const {
    return childIds;
}

ProcessState ProcessDescriptor::getState() const {
    return state;
}

uint8_t ProcessDescriptor::getPriority() const {
    return priority;
}

const char *ProcessDescriptor::getTitle() const {
    return title;
}

int32_t ProcessDescriptor::getR1() const {
    return r1;
}

int32_t ProcessDescriptor::getR2() const {
    return r2;
}

int32_t ProcessDescriptor::getRr() const {
    return rr;
}

uint16_t ProcessDescriptor::getIc() const {
    return ic;
}

uint32_t ProcessDescriptor::getC() const {
    return c;
}

uint8_t ProcessDescriptor::getSs() const {
    return ss;
}

uint8_t ProcessDescriptor::getCf() const {
    return cf;
}

PtrUnion ProcessDescriptor::getPageTable() const {
    return pageTable;
}

void ProcessDescriptor::setState(ProcessState state) {
    ProcessDescriptor::state = state;
}

void ProcessDescriptor::setPriority(uint8_t priority) {
    ProcessDescriptor::priority = priority;
}

void ProcessDescriptor::setR1(int32_t r1) {
    ProcessDescriptor::r1 = r1;
}

void ProcessDescriptor::setR2(int32_t r2) {
    ProcessDescriptor::r2 = r2;
}

void ProcessDescriptor::setRr(int32_t rr) {
    ProcessDescriptor::rr = rr;
}

void ProcessDescriptor::setIc(uint16_t ic) {
    ProcessDescriptor::ic = ic;
}

void ProcessDescriptor::setC(uint32_t c) {
    ProcessDescriptor::c = c;
}

void ProcessDescriptor::setSs(uint8_t ss) {
    ProcessDescriptor::ss = ss;
}

void ProcessDescriptor::setCf(uint8_t cf) {
    ProcessDescriptor::cf = cf;
}

void ProcessDescriptor::setPageTable(const PtrUnion &pageTable) {
    ProcessDescriptor::pageTable = pageTable;
}

resource_id_t ProcessDescriptor::getWaitingResourceId() const {
    return waitingResourceId;
}

void ProcessDescriptor::setWaitingResourceId(resource_id_t waitingResourceId) {
    ProcessDescriptor::waitingResourceId = waitingResourceId;
}

Process::Process(ProcessManager *manager)
        : manager(manager) {}

process_id_t Process::getId() const {
    return this->id;
}

ProcessManager *Process::getManager() const {
    return manager;
}

ResourceManager *Process::getResourceManager() const {
    return manager->getMachine()->getResourceManager();
}

ProcessManager::ProcessManager(RealMachine *machine)
        : machine(machine) {
    this->initPriorityLine();
}

ProcessManager::~ProcessManager() {
    for (auto process: this->processes)
        delete process.second;
};

RealMachine *ProcessManager::getMachine() const {
    return machine;
}

process_id_t ProcessManager::registerProcess(Process *process, uint8_t priority, const char *title, process_id_t id,
                                             process_id_t parentId) {
    if (id == 0xFF)
        id = this->findFirstUnusedProcessId();

    cout << "Užregistruotas procesas " << title << " (" << (int) id << ") su prioritetu: " << (int) priority << endl;

    process->id = id;
    this->addProcessToPriorityLine(id, priority);
    this->processes[id] = process;

    auto descriptor = ProcessDescriptor(priority, title, id, parentId);
    this->writeDescriptorIntoMemory(descriptor);

    if (parentId != 0xFF)
        this->appendProcessToParent(id, parentId);

    return id;
}

void ProcessManager::pauseProcess(process_id_t pid) {
    auto desc = this->readDescriptorFromMemory(pid);

    auto status = desc.getState();
    if(status == RUNNING)
        throw std::runtime_error("Pause process was called on active process!");

    if(status == READY)
        desc.setState(READY_STOPPED);

    else if(status == BLOCKED)
        desc.setState(BLOCKED_STOPPED);

    this->writeDescriptorIntoMemory(desc);
}

void ProcessManager::unpauseProcess(process_id_t pid) {
    auto desc = this->readDescriptorFromMemory(pid);

    auto status = desc.getState();
    if(status == RUNNING)
        throw std::runtime_error("Pause process was called on active process!");

    if(status == READY)
        desc.setState(READY_STOPPED);

    else if(status == BLOCKED)
        desc.setState(BLOCKED_STOPPED);

    this->writeDescriptorIntoMemory(desc);
}

void ProcessManager::unregisterProcess(process_id_t pid) {
    auto descriptor = this->readDescriptorFromMemory(pid);
    for (uint8_t i = 0; i < MAX_PROCESS_COUNT; i++) {
        auto cpid = descriptor.getChildIds()[i];

        if (cpid != 0xFF)
            unregisterProcess(cpid);
    }

    auto resourceManager = this->getMachine()->getResourceManager();

    //Remove from resource waiting list.
    if (descriptor.getState() == BLOCKED || descriptor.getState() == BLOCKED_STOPPED) {
        auto rid = descriptor.getWaitingResourceId();
        try {
            auto rdesc = resourceManager->readDescriptorFromMemory(rid);
            rdesc.removeWaitingByValue(pid);
            resourceManager->writeDescriptorIntoMemory(rdesc);
        } catch (std::out_of_range const &err) {
            //If the resource is already deleted we dont care.
        }
    }

    //Removes all resources created by this process.
    for (uint8_t i = 0; i < MAX_RESOURCES_COUNT; i++) {
        try {
            auto rdesc = resourceManager->readDescriptorFromMemory(i);
            if (rdesc.getOwnerId() == pid)
                resourceManager->unregisterResource(i);
        } catch (std::out_of_range const &e) {
            continue;
        }
    }

    //Removes from the parent descriptor.
    if (descriptor.getParentId() != 0xFF) {
        auto parentDesc = this->readDescriptorFromMemory(descriptor.getParentId());
        parentDesc.removeChildProcessByValue(pid);
        this->writeDescriptorIntoMemory(parentDesc);
    }

    //Removes the process from priority line.
    this->removeProcessFromPriorityLineByValue(pid);

    //Removes the descriptor.
    this->removeDescriptorFromMemory(pid);

    //Removes the process.
    delete this->processes.at(pid);
    this->processes.erase(pid);
}

void ProcessManager::initPriorityLine() {
    auto memory = this->getMachine()->getMemory()->getSupervisorMemory();
    auto block = memory->readBlock(SUPERVISOR_PROCESS_PRIORITY_ARRAY_OFFSET);
    auto blockU8 = reinterpret_cast<uint8_t *>(block.data());

    for (uint8_t i = 0; i < MAX_PROCESS_COUNT; i++)
        blockU8[i] = 0xFF;

    memory->writeBlock(SUPERVISOR_PROCESS_PRIORITY_ARRAY_OFFSET, block);
}

process_id_t ProcessManager::findNextProcessToExecute() const {
    auto memory = this->getMachine()->getMemory()->getSupervisorMemory();
    auto block = memory->readBlock(SUPERVISOR_PROCESS_PRIORITY_ARRAY_OFFSET);
    auto blockU8 = reinterpret_cast<uint8_t *>(block.data());

    //TODO: randomize.
    for (size_t i = 0; i < MAX_PROCESS_COUNT; i++) {
        auto pid = blockU8[i];
        if (pid == 0xFF)
            break;

        auto descriptor = this->readDescriptorFromMemory(pid);
        if (descriptor.getState() == READY)
            return pid;
    }

    throw std::runtime_error("No processes found that can be executed!");
}

void ProcessManager::addProcessToPriorityLine(process_id_t id, uint8_t priority) {
    auto memory = this->getMachine()->getMemory()->getSupervisorMemory();
    auto block = memory->readBlock(SUPERVISOR_PROCESS_PRIORITY_ARRAY_OFFSET);
    auto blockU8 = reinterpret_cast<uint8_t *>(block.data());

    bool inserted = false;
    for (uint8_t i = 0; i < MAX_PROCESS_COUNT; i++) {
        if (blockU8[i] == 0xFF) {
            blockU8[i] = id;
            inserted = true;
            break;
        }

        auto pid = blockU8[i];
        auto descriptor = this->readDescriptorFromMemory(pid);
        if (descriptor.getPriority() < priority) {
            //insertion sort is the way to go.
            for (uint8_t j = MAX_PROCESS_COUNT - 1; j-- > i;)
                blockU8[j + 1] = blockU8[j];
            blockU8[i] = id;
            inserted = true;
            break;
        }
    }

    if (!inserted)
        throw std::runtime_error("Failed to add process to priority line!");

    memory->writeBlock(SUPERVISOR_PROCESS_PRIORITY_ARRAY_OFFSET, block);
}

void ProcessManager::removeProcessFromPriorityLine(uint8_t index) {
    auto memory = this->getMachine()->getMemory()->getSupervisorMemory();
    auto block = memory->readBlock(SUPERVISOR_PROCESS_PRIORITY_ARRAY_OFFSET);
    auto blockU8 = reinterpret_cast<uint8_t *>(block.data());

    for (uint8_t i = index; i < MAX_PROCESS_COUNT - 1; i++)
        blockU8[i] = blockU8[i + 1];

    blockU8[MAX_PROCESS_COUNT - 1] = 0xFF;

    memory->writeBlock(SUPERVISOR_PROCESS_PRIORITY_ARRAY_OFFSET, block);
}

void ProcessManager::removeProcessFromPriorityLineByValue(process_id_t pid) {
    auto memory = this->getMachine()->getMemory()->getSupervisorMemory();
    auto block = memory->readBlock(SUPERVISOR_PROCESS_PRIORITY_ARRAY_OFFSET);
    auto blockU8 = reinterpret_cast<uint8_t *>(block.data());

    for (uint8_t i = 0; i < MAX_PROCESS_COUNT; i++)
        if (blockU8[i] == pid)
            this->removeProcessFromPriorityLine(i);
}

process_id_t ProcessManager::findFirstUnusedProcessId() const {
    for (process_id_t i = 0; i < SUPERVISOR_PROCESS_DESCRIPTOR_SIZE; i++)
        if (!this->isDescriptorSlotUsed(i))
            return i;

    throw std::runtime_error("There are no free process descriptors to allocate!");
}

void ProcessManager::writeDescriptorIntoMemory(const ProcessDescriptor &descriptor) const {
    auto block = descriptor.toBlock();
    auto memory = this->getMachine()->getMemory();

    memory->getSupervisorMemory()->writeBlock(SUPERVISOR_PROCESS_DESCRIPTOR_OFFSET + descriptor.getId(), block);
}

void ProcessManager::removeDescriptorFromMemory(process_id_t id) const {
    memory_block_t block = {0};
    auto memory = this->getMachine()->getMemory();

    memory->getSupervisorMemory()->writeBlock(SUPERVISOR_PROCESS_DESCRIPTOR_OFFSET + id, block);
}

Process *ProcessManager::getProcessById(process_id_t id) const {
    return this->processes.at(id);
}

void ProcessManager::print() const {
    cout << "Printing processes..." << endl;

    for (process_id_t i = 0; i < MAX_PROCESS_COUNT; i++) {
        cout << '[' << setw(2) << (int) i << "] ";

        try {
            auto descriptor = this->readDescriptorFromMemory(i);

            cout << descriptor.getTitle() << ": ";

            auto parentId = descriptor.getParentId();
            if (parentId != 0xFF)
                cout << "parent: " << (int) parentId << ", ";

            cout << "priority: " << (int) descriptor.getPriority() << ", ";

            auto state = descriptor.getState();
            cout << "state: " << convertProcessStateToString(state);
            if(state == BLOCKED || state == BLOCKED_STOPPED)
                cout << ", waiting: " << (int) descriptor.getWaitingResourceId();

            cout << endl;
        } catch (std::out_of_range const &e) {
            cout << "Nenaudojamas!" << endl;
        }
    }
}

ProcessDescriptor ProcessManager::readDescriptorFromMemory(process_id_t id) const {
    auto memory = this->getMachine()->getMemory();
    auto block = memory->getSupervisorMemory()->readBlock(SUPERVISOR_PROCESS_DESCRIPTOR_OFFSET + id);

    if (Memory::isMemoryBlockZeroed(block))
        throw std::out_of_range("ProcessManager::readDescriptorFromMemory was called with unused id!");

    return ProcessDescriptor(block);
}

bool ProcessManager::isDescriptorSlotUsed(process_id_t id) const {
    auto memory = this->getMachine()->getMemory();
    auto block = memory->getSupervisorMemory()->readBlock(SUPERVISOR_PROCESS_DESCRIPTOR_OFFSET + id);

    return !Memory::isMemoryBlockZeroed(block);
}

void ProcessManager::appendProcessToParent(process_id_t id, process_id_t parentId) const {
    auto descriptor = this->readDescriptorFromMemory(parentId);
    descriptor.addChildProcess(id);
    this->writeDescriptorIntoMemory(descriptor);
}

void ProcessManager::runPlanner() {
    process_id_t processIdToExecute = this->findNextProcessToExecute();
    auto process = this->processes.at(processIdToExecute);

    cout << "Procesu planuotojas pasirinko paleisti procesa id: " << (int) processIdToExecute << endl;

    auto descriptor = this->readDescriptorFromMemory(processIdToExecute);
    descriptor.setState(RUNNING);
    this->writeDescriptorIntoMemory(descriptor);

    process->run();

    descriptor = this->readDescriptorFromMemory(processIdToExecute);
    if (descriptor.getState() == RUNNING)
        descriptor.setState(READY);
    this->writeDescriptorIntoMemory(descriptor);
}