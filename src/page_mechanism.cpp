//
// Created by kudze on 2022-03-27.
//

#include "page_mechanism.h"


PageMechanism::PageMechanism(Memory *memory) : memory(memory) {
    this->initPageSegmentMemory();
}

PageMechanism::~PageMechanism() {

}

void PageMechanism::initPageSegmentMemory() {
    auto memory = this->getMemory();

    for (block_address_t i = SUPERVISOR_PAGE_SEGMENT_OFFSET;
         i < SUPERVISOR_PAGE_SEGMENT_OFFSET + SUPERVISOR_PAGE_SEGMENT_SIZE; i++)
        for (word_address_t j = 0; j < MEMORY_BLOCK_SIZE; j++)
            memory->getSupervisorMemory()->writeWord(i, j, 0);
}

PtrUnion PageMechanism::allocateVirtualMachineMemory() const {
    auto ptr = this->getFirstFreePageTableAddress();
    memory_word_t pageTable[3];
    auto pageTablePtr = reinterpret_cast<uint8_t*>(pageTable);

    for(int i = 0; i < VIRTUAL_MEMORY_TOTAL_BLOCKS; i++)
        while(true) {
            block_address_t blockAddress = rand() % MEMORY_USER_BLOCK_COUNT;

            for(int j = 0; j < i; j++) {
                if (pageTablePtr[j] == blockAddress)
                    continue;
            }

            if(this->isUserBlockUsed(blockAddress))
                continue;

            pageTablePtr[i] = blockAddress;
            break;
        }

    auto offset = ptr.getExpanded().realBlockAddressSize * MEMORY_BLOCK_SIZE + ptr.getExpanded().realWordAddressSize;
    for(int i = 0; i < VIRTUAL_MEMORY_TOTAL_BLOCKS / 4; i++)
        this->getMemory()->getSupervisorMemory()->writeWordCombined(offset + i, pageTable[i]);

    return ptr;
}

PtrUnion PageMechanism::getFirstFreePageTableAddress() const {
    for (vm_index_t i = 0; i < MAX_VMS; i++) {
        auto ptr = this->getNthPageTableAddress(i);

        if(!this->isPageTableAddressUsed(ptr))
            return ptr;
    }

    throw std::runtime_error("Memory::getFirstFreePageTableAddress was called when there is no free page tables!");
}

PtrUnion PageMechanism::getNthPageTableAddress(vm_index_t index) const {
    return PtrUnion(
            0,
            VIRTUAL_MEMORY_TOTAL_BLOCKS,
            SUPERVISOR_PAGE_SEGMENT_OFFSET + (index / 3),
            (index * 3) % 16
    );
}

bool PageMechanism::isPageTableAddressUsed(PtrUnion ptr) const {
    auto offset = static_cast<uint16_t>(ptr.getExpanded().realBlockAddressSize) * MEMORY_BLOCK_SIZE + ptr.getExpanded().realWordAddressSize;
    auto mem1 = memory->getSupervisorMemory()->readWordCombined(offset);
    auto mem2 = memory->getSupervisorMemory()->readWordCombined(offset + 1);
    auto mem3 = memory->getSupervisorMemory()->readWordCombined(offset + 2);

    return mem1 != 0 || mem2 != 0 || mem3 != 0;
}

bool PageMechanism::isUserBlockUsed(block_address_t blockAddress) const {

    for(vm_index_t i = 0; i < MAX_VMS; i++)
    {
        auto ptr = this->getNthPageTableAddress(i);
        if(!this->isPageTableAddressUsed(ptr))
            continue;

        memory_word_t pageTable[3];
        auto offset = static_cast<uint16_t>(ptr.getExpanded().realBlockAddressSize) * MEMORY_BLOCK_SIZE + ptr.getExpanded().realWordAddressSize;
        pageTable[0] = memory->getSupervisorMemory()->readWordCombined(offset);
        pageTable[1] = memory->getSupervisorMemory()->readWordCombined(offset + 1);
        pageTable[2] = memory->getSupervisorMemory()->readWordCombined(offset + 2);

        auto pageTablePtr = reinterpret_cast<uint8_t*>(pageTable);
        for(uint8_t j = 0; j < VIRTUAL_MEMORY_TOTAL_BLOCKS; j++)
            if(pageTablePtr[j] == blockAddress)
                return true;
    }

    return false;
}

block_address_t PageMechanism::getRealBlockFromVirtualBlock(const PtrUnion &ptr, block_address_t virtualBlockAddress) const {
    if(virtualBlockAddress >= VIRTUAL_MEMORY_TOTAL_BLOCKS)
        throw std::runtime_error("PageMechanism::getRealblockFromVirtualBlock was called with block address that is out of bounds!");

    memory_word_t pageTableCell = this->getMemory()->getSupervisorMemory()->readWordCombined(
            ptr.getExpanded().realBlockAddressSize * MEMORY_BLOCK_SIZE +
            ptr.getExpanded().realWordAddressSize + (virtualBlockAddress / 4)
            );

    auto pageTableCellPtr = reinterpret_cast<block_address_t*>(&pageTableCell);
    return pageTableCellPtr[3 - virtualBlockAddress % 4];
}

Memory *PageMechanism::getMemory() const {
    return memory;
}
