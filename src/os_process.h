//
// Created by kudze on 2022-05-29.
//

#ifndef LAB04_OS_PROCESS_H
#define LAB04_OS_PROCESS_H

#define PROCESS_BOOT_ID 0
#define PROCESS_SHUTDOWN_ID 1
#define PROCESS_INSTALL_ID 2
#define PROCESS_MAIN_ID 3
#define PROCESS_TERMINAL_ID 4
#define PROCESS_INTERRUPT_ID 5
#define PROCESS_GET_PUT_DATA_ID 6

#define RESOURCE_MACHINE_STOP_ID 0 //Mašina turėtų būti įjungta resursas.
#define RESOURCE_INSTALL_ID 1 //"Programa iš įvedimo irenginio” resursas.
#define RESOURCE_CHANNEL_REGISTER_ID 2 //“Kanalų įrenginio” resursas
#define RESOURCE_MEMORY_TASK_ID 3 //"Užduotis atmintyje"
#define RESOURCE_JG_LOAD_TASK_ID 4 //"JG Krauk"
#define RESOURCE_JG_INTERRUPT_ID 5 //"JG Pertraukimas"
#define RESOURCE_INTERRUPT_ID 6 //"Pertraukimas"

#include "process.h"

/**
 * System processes.
 */

class BootProcess : public Process {
    bool shutdown = false;

    void createStaticResources();
    void createSystemProcesses();
    void destroyStaticResources();
    void destroySystemProcesses();

public:
    explicit BootProcess(ProcessManager* manager);

    virtual void run();
    virtual void acceptResourceElement(resource_id_t resourceId, uint8_t data, process_id_t senderId);

};

class ShutdownProcess : public Process {
public:
    explicit ShutdownProcess(ProcessManager* manager);

    virtual void run();
    virtual void acceptResourceElement(resource_id_t resourceId, uint8_t data, process_id_t senderId) {};
};

#define MAX_INSTALLABLE_SLOTS 16

class InstallProcess : public Process {
    uint8_t installToSlot;
    bool hasChannelRegister;

public:
    static bool isHeaderValid(memory_block_t header);

    explicit InstallProcess(ProcessManager* manager);

    virtual void run();
    virtual void acceptResourceElement(resource_id_t resourceId, uint8_t data, process_id_t senderId);
};

class MainProcess : public Process {
    uint8_t slot = 0xFF;
    uint8_t sender = 0xFF;

public:
    explicit MainProcess(ProcessManager* manager);

    virtual void run();
    virtual void acceptResourceElement(resource_id_t resourceId, uint8_t data, process_id_t senderId);
};

class JobGovernorProcess : public Process {
    uint8_t slot = 0xFF;
    bool hasChannelManager = false;
    uint8_t vmid = 0xFF;
    uint8_t jgInterrupt = 0xFF;

    void printLine(std::string const& line);
    bool boot();
public:
    explicit JobGovernorProcess(ProcessManager* manager);

    virtual void run();
    virtual void acceptResourceElement(resource_id_t resourceId, uint8_t data, process_id_t senderId);
};

class VMProcess : public Process {
public:
    explicit VMProcess(ProcessManager* manager);

    virtual void run();
    virtual void acceptResourceElement(resource_id_t resourceId, uint8_t data, process_id_t senderId);
};

class InterruptService : public Process {
    uint8_t senderPID = 0xFF;
    uint8_t senderSI = 0xFF;

public:
    explicit InterruptService(ProcessManager* manager);

    virtual void run();
    virtual void acceptResourceElement(resource_id_t resourceId, uint8_t data, process_id_t senderId);
};

class TerminalProcess : public Process {
    bool hasChannelRegister;

    bool runCommand(memory_block_t commandBlock);

    void printHelp();
    void printLine(std::string const& line);

public:
    explicit TerminalProcess(ProcessManager* manager);

    virtual void run();
    virtual void acceptResourceElement(resource_id_t resourceId, uint8_t data, process_id_t senderId);
};

#endif //LAB04_OS_PROCESS_H
