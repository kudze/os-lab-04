//
// Created by kudze on 2022-05-29.
//

#ifndef LAB04_PROCESS_H
#define LAB04_PROCESS_H

#define MAX_PROCESS_COUNT 16

#include <unordered_map>

class ProcessManager;
class Process;
typedef uint8_t process_id_t;

#include "resource.h"
#include "real_machine.h"

enum ProcessState {
    RUNNING,        //Vykdomas
    READY,          //Pasiruošęs
    BLOCKED,        //Blokuotas
    READY_STOPPED,  //Pasiruošęs stabdytas
    BLOCKED_STOPPED, //Blokuotas stabdytas
};

std::string convertProcessStateToString(ProcessState state);

class ProcessDescriptor {
    process_id_t id;
    process_id_t parentId;
    process_id_t childIds[16] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};

    ProcessState state;
    uint8_t priority;
    char title[11] = "";

    //REGISTERS
    int32_t r1 = 0;
    int32_t r2 = 0;
    int32_t rr = 0;
    uint16_t ic = 0;
    uint32_t c = 0;
    uint8_t ss = 0;
    uint8_t cf = 0;
    PtrUnion pageTable = PtrUnion(0);
    //REGISTERS END

    resource_id_t waitingResourceId = 0xFF;

public:
    ProcessDescriptor(uint8_t priority, const char* title, process_id_t id, process_id_t parentId = 0xFF);
    explicit ProcessDescriptor(memory_block_t block);

    void addChildProcess(process_id_t childId);
    void removeChildProcess(uint8_t index);
    void removeChildProcessByValue(process_id_t childId);

    memory_block_t toBlock() const;

    process_id_t getId() const;
    process_id_t getParentId() const;
    const process_id_t *getChildIds() const;
    ProcessState getState() const;
    uint8_t getPriority() const;
    const char *getTitle() const;
    int32_t getR1() const;
    int32_t getR2() const;
    int32_t getRr() const;
    uint16_t getIc() const;
    uint32_t getC() const;
    uint8_t getSs() const;
    uint8_t getCf() const;
    PtrUnion getPageTable() const;
    resource_id_t getWaitingResourceId() const;

    void setState(ProcessState state);
    void setPriority(uint8_t priority);
    void setR1(int32_t r1);
    void setR2(int32_t r2);
    void setRr(int32_t rr);
    void setIc(uint16_t ic);
    void setC(uint32_t c);
    void setSs(uint8_t ss);
    void setCf(uint8_t cf);
    void setPageTable(const PtrUnion &pageTable);
    void setWaitingResourceId(resource_id_t waitingResourceId);
};

class Process {
    ProcessManager* manager;
    process_id_t id;
    friend class ProcessManager;

public:
    explicit Process(ProcessManager* manager);
    virtual ~Process() = default;

    virtual void run() = 0;
    virtual void acceptResourceElement(resource_id_t resourceId, uint8_t data, process_id_t senderId) = 0;

    ProcessManager *getManager() const;
    ResourceManager *getResourceManager() const;
    process_id_t getId() const;
};

class ProcessManager {
    RealMachine* machine;

    std::unordered_map<process_id_t, Process*> processes;

    process_id_t findNextProcessToExecute() const;
    void initPriorityLine();
    void addProcessToPriorityLine(process_id_t id, uint8_t priority);
    void removeProcessFromPriorityLine(uint8_t index);
    void removeProcessFromPriorityLineByValue(process_id_t pid);

    process_id_t findFirstUnusedProcessId() const;
    void appendProcessToParent(process_id_t id, process_id_t parentId) const;

public:
    explicit ProcessManager(RealMachine* machine);
    ~ProcessManager();

    process_id_t registerProcess(Process* process, uint8_t priority, const char* title, process_id_t id = 0xFF, process_id_t parentId = 0xFF);
    void unregisterProcess(process_id_t process);

    void pauseProcess(process_id_t pid);
    void unpauseProcess(process_id_t pid);

    void runPlanner();

    ProcessDescriptor readDescriptorFromMemory(process_id_t id) const;
    void writeDescriptorIntoMemory(ProcessDescriptor const& descriptor) const;
    void removeDescriptorFromMemory(process_id_t id) const;
    bool isDescriptorSlotUsed(process_id_t id) const;

    Process* getProcessById(process_id_t id) const;

    void print() const;

    RealMachine *getMachine() const;
};

#endif //LAB04_PROCESS_H
